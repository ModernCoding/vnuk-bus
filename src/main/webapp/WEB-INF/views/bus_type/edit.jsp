<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Update bus type n� ${bus_type.id}</h3>
    <form action="../update" method="post">
        <h1>Add a new bus type</h1>
		<hr/>
		<input type="hidden" name="id" value="${bus_type.id}" />
		
		<b>Label :</b> 
			<textarea name="label" cols="50">${bus_type.label}</textarea>	<br/>
		<b>Information :</b> 
			<textarea name="information" cols="50">${bus_type.information}</textarea> <br/>
	
		<hr/>			
		<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../../bus_types">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close-bus-types.jsp" />
