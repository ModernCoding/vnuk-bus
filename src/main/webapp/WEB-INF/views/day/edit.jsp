<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Update day n� ${day.id}</h3>
    <form action="../update" method="post">
        <h1>Add a new day</h1>
		<hr/>
		<input type="hidden" name="id" value="${day.id}" />
		
		<b>Name :</b> 
			<textarea name="name" cols="50">${day.name}</textarea>	<br/>
		<hr/>			
		<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../../days">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close-days.jsp" />