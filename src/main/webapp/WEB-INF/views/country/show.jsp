<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Name</th>
            
            </tr>
        </thead>

        <tbody>

	        <tr>
	            <td>
	                <a href="edit/${country.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${country.name}" 
	                        data-placement="bottom"
                        >
                    	<i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
	
	            </td>
	
	            <td>${country.name}</td>
	           
	        </tr>

        </tbody>
    </table>
    <a class="btn btn-default" href="../countries">
	    Back
	</a>
            
<c:import url="/resources/jsp/body-close-countries.jsp" />
