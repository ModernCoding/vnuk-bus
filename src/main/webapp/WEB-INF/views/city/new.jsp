<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Create city</h3>
    <form:errors path="city.name" cssStyle="color: red;"/><br/>
    <form:errors path="city.countryId" cssStyle="color: red;"/>
    <form action="create" method="post">
        <h1>Add a new city</h1>
		<hr/>
		<table class="table table-bordered table-hover table-responsive table-striped">
			<tbody>
				<tr>
					
                    <td>
                    	<input type="hidden" name="id" value="${city.id}" />
						Name: <input type="text" name="name"/><br/>
					</td>
					
					<td>
						Country ID: <br/>
						<form:select path = "city.countryId">
				        	<form:options items = "${countryList}" />
				        </form:select>
					</td>

					
				</tr>
			</tbody>
		</table>	
				<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../cities">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
