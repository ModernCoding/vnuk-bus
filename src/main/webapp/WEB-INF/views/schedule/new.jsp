<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Create Bus Manufacture</h3>
    <form:errors path="schedule.name" cssStyle="color: red;"/>
    <form action="create" method="post">
        <h1>Add a new schedule</h1>
		<hr/>
		<table class="table table-bordered table-hover table-responsive table-striped">
			<tbody>
				<tr>
					
                    <td>
                    	<input type="hidden" name="id" value="${schedule.id}" />
						Name: <input type="text" name="name"/><br/>
					</td>
					
					 <td>
                    	
						RouteID: <input type="text" name="route_id"/><br/>
					</td>
					
					 <td>
						DayID: <input type="text" name="day_id"/><br/>
					</td>
					
					 <td>
						DepartureTime: <input type="text" name="departure_time"/><br/>
					</td>
					
					 <td>
						ArrivalTime: <input type="text" name="arrival_time"/><br/>
					</td>
					
					
				</tr>
			</tbody>
		</table>	
				<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../schedules">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
