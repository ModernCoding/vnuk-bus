<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />

    <a href="schedule/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Bus Manufacture
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>RouteID</th>
                <th>DayID</th>
                <th>DepartureTime</th>
                <th>ArrivalTime</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="schedule" items="${schedules}">
            
                <tr>
                    <td>                     
                        <a href="schedule/${schedule.id}"
	                        class="btn btn-xs btn-default" 
	                        data-toggle="tooltip" 
	                        title="Show ${schedule.route_id}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-eye" aria-hidden="true"></i>
                        </a> 
                        
                        <a href="schedule/edit/${schedule.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${schedule.route_id}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-pencil" aria-hidden="true"></i>
                        </a> 
                        
                        <button type="button"
                                class="btn btn-xs btn-danger my-bus-manufacture-to-delete" 
                                value="${schedule.id}"
                                data-toggle="tooltip" 
                                title="Delete ${schedule.route_id}" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>

                    <td>${schedule.route_id}</td>
                    <td>${schedule.day_id}</td>
                    <td>${schedule.departure_time}</td>
                    <td>${schedule.arrival_time}</td>
                   
                </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-schedules.jsp" />
