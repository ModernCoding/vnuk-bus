<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Update customer n� ${customer.id}</h3>
    <form action="../update" method="post">
        <h1>Add a new contact</h1>
		<hr/>
		<input type="hidden" name="id" value="${customer.id}" />
		
		<b>Name :</b> 
			<textarea name="name" cols="50">${customer.name}</textarea>	<br/>
		<b>Email :</b> 
			<textarea name="email" cols="50">${customer.email}</textarea> <br/>
		<b>Phone :</b> 
			<textarea name="phone" cols="50">${customer.phone}</textarea>
		<hr/>			
		<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../../customers">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close-customers.jsp" />

