<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Update season n� ${season.id}</h3>
    <form action="../update" method="post">
        <h1>Add a new contact</h1>
		<hr/>
		<input type="hidden" name="id" value="${season.id}" />
		
		<b>Label :</b> 
			<textarea name="label" cols="50">${season.label}</textarea>	<br/>
		<b>Information :</b> 
			<textarea name="information" cols="50">${season.information}</textarea> <br/>
			
		<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../../seasons">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close-seasons.jsp" />
