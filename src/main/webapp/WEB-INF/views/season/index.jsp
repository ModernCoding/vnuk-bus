<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />

    <a href="season/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New season
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Label</th>
                <th>Information</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="season" items="${seasons}">
            
                <tr>
                    <td>                     
                        <a href="season/${season.id}"
	                        class="btn btn-xs btn-default" 
	                        data-toggle="tooltip" 
	                        title="Show ${season.label}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-eye" aria-hidden="true"></i>
                        </a> 
                        
                        <a href="season/edit/${season.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${season.label}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-pencil" aria-hidden="true"></i>
                        </a> 
                        
                        <button type="button"
                                class="btn btn-xs btn-danger my-season-to-delete" 
                                value="${season.id}"
                                data-toggle="tooltip" 
                                title="Delete ${season.label}" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>

                    <td>${season.label}</td>
                    <td>${season.information}</td>                   
                </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-seasons.jsp" />
