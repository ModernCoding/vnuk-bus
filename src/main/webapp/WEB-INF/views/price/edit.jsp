<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Update price n� ${price.id}</h3>
    <form action="../update" method="post">
        <h1>Add a new price</h1>
		<hr/>
		<input type="hidden" name="id" value="${price.id}" />
		
		<b>RouteID:</b> 
			<textarea name="name" cols="50">${price.route_id}</textarea>	<br/>
		
		<b>SeasonID :</b> 
			<textarea name="name" cols="50">${price.season_id}</textarea>	<br/>
		
		<b>Price :</b> 
			<textarea name="name" cols="50">${price.price}</textarea>	<br/>		
		<hr/>			
		<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../../prices">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close-prices.jsp" />
