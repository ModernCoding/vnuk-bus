<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>RouteID</th>
                <th>SeasonID</th>
                <th>Prices</th>
            
            </tr>
        </thead>

        <tbody>

	        <tr>
	            <td>
	                <a href="edit/${price.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${price.route_id}" 
	                        data-placement="bottom"
                        >
                    	<i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
	
	            </td>
	
	            <td>${price.route_id}</td>
                <td>${price.season_id}</td>
                <td>${price.price}</td>
	           
	        </tr>

        </tbody>
    </table>
    <a class="btn btn-default" href="../prices">
	    Back
	</a>
            
<c:import url="/resources/jsp/body-close-prices.jsp" />
