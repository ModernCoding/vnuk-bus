<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />

    <a href="price/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New price
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>RouteID</th>
                <th>SeasonID</th>
                <th>Prices</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="price" items="${prices}">
            
                <tr>
                    <td>                     
                        <a href="price/${price.id}"
	                        class="btn btn-xs btn-default" 
	                        data-toggle="tooltip" 
	                        title="Show ${price.route_id}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-eye" aria-hidden="true"></i>
                        </a> 
                        
                        <a href="price/edit/${price.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${price.route_id}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-pencil" aria-hidden="true"></i>
                        </a> 
                        
                        <button type="button"
                                class="btn btn-xs btn-danger my-price-to-delete" 
                                value="${price.id}"
                                data-toggle="tooltip" 
                                title="Delete ${price.route_id}" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>

                    <td>${price.route_id}</td>
                    <td>${price.season_id}</td>
                    <td>${price.price}</td>
                </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-prices.jsp" />
