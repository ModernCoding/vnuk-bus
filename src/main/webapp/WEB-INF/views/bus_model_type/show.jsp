<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>BusTypeID</th>
                <th>BusModelID</th>
                <th>PhotoID</th>
            </tr>
        </thead>

        <tbody>

	        <tr>
	            <td>
	                <a href="edit/${bus_model_type.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${bus_model_type.bus_type_id}" 
	                        data-placement="bottom"
                        >
                    	<i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>

	            </td>
	
	            <td>${bus_model_type.bus_type_id}</td>
	            <td>${bus_model_type.bus_model_id}</td>
	            <td>${bus_model_type.photo_id}</td>
	        </tr>

        </tbody>
    </table>
    <a class="btn btn-default" href="../bus_model_types">
	    Back
	</a>
            
<c:import url="/resources/jsp/body-close-bus-model-types.jsp" />
