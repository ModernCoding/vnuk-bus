<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Update Bus Manufacture n� ${bus_model_type.id}</h3>
    <form action="../update" method="post">
        <h1>Add a new contact</h1>
		<hr/>
		<input type="hidden" name="id" value="${bus_model_type.id}" />
		
		<b>BusTypeID :</b> 
			<textarea name="bustypeid" cols="50">${bus_model_type.bus_type_id}</textarea>
		<b>BusModelID :</b> 
			<textarea name="busmodelid" cols="50">${bus_model_type.bus_mode_lid}</textarea>
		<b>PhotoID :</b> 
			<textarea name="photoid" cols="50">${bus_model_type.photo_id}</textarea>	<br/>
		<hr/>			
		<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../../bus_model_types">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close-bus-model-types.jsp" />
