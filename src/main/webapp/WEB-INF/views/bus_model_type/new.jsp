<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Create Bus Model Type</h3>
    <form:errors path="busModelType.bus_type_id" cssStyle="color: red;"/><br/>
    <form:errors path="busModelType.bus_model_id" cssStyle="color: red;"/><br/>
    <form:errors path="busModelType.photo_id" cssStyle="color: red;"/><br/>
    <form action="create" method="post">
        <h1>Add a new bus model type</h1>
		<hr/>
		<table class="table table-bordered table-hover table-responsive table-striped">
			<tbody>
				<tr>
					
                     <td>
                    	<input type="hidden" name="id" value="${bus_model_type.id}" />
						BusTypeID: <br/>
						<form:select path = "bus_model_type.busTypeId">
				        	<form:options items = "${busTypeList}" />
				        </form:select>
					</td>
					
					<td>
						
						BusModelID: <br/>
						<form:select path = "bus_model_type.busModelId">
				        	<form:options items = "${busModelList}" />
				        </form:select>
					</td>
					<td>
						
						PhotoID: <br/>
						<form:select path = "bus_model_type.photoId">
				        	<form:options items = "${photoList}" />
				        </form:select>
					</td>
					
					
				</tr>
			</tbody>
		</table>	
				<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../bus_model_types">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
