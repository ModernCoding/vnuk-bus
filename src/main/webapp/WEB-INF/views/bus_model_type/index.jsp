<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />

    <a href="bus_model_type/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Bus Model Type
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>BusTypeID</th>
                <th>BusModelID</th>
                <th>PhotoID</th>
               
            </tr>
        </thead>

        <tbody>

            <c:forEach var="bus_model_type" items="${bus_model_types}">
            
                <tr>
                    <td>                     
                        <a href="bus_model_type/${bus_model_type.id}"
	                        class="btn btn-xs btn-default" 
	                        data-toggle="tooltip" 
	                        title="Show ${bus_model_type.bus_type_id}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-eye" aria-hidden="true"></i>
                        </a> 
                        
                        <a href="bus_model_type/edit/${bus_model_type.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${bus_model_type.bus_type_id}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-pencil" aria-hidden="true"></i>
                        </a> 
                        
                        <button type="button"
                                class="btn btn-xs btn-danger my-bus-manufacture-to-delete" 
                                value="${bus_model_type.id}"
                                data-toggle="tooltip" 
                                title="Delete ${bus_model_type.bus_type_id}" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>

                        <td>${bus_model_type.bus_type_id}</td>
			            <td>${bus_model_type.bus_model_id}</td>
			            <td>${bus_model_type.photo_id}</td>
                </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-bus-model-types.jsp" />
