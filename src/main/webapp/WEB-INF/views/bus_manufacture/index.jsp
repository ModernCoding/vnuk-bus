<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />

    <a href="bus_manufacture/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Bus Manufacture
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Name</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="bus_manufacture" items="${bus_manufactures}">
            
                <tr>
                    <td>                     
                        <a href="bus_manufacture/${bus_manufacture.id}"
	                        class="btn btn-xs btn-default" 
	                        data-toggle="tooltip" 
	                        title="Show ${bus_manufacture.name}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-eye" aria-hidden="true"></i>
                        </a> 
                        
                        <a href="bus_manufacture/edit/${bus_manufacture.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${bus_manufacture.name}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-pencil" aria-hidden="true"></i>
                        </a> 
                        
                        <c:if test="${bus_manufacture.numberOfBusModels=='0'}">
	                        <button type="button"
	                                class="btn btn-xs btn-danger my-bus-manufacture-to-delete" 
	                                value="${bus_manufacture.id}"
	                                data-toggle="tooltip" 
	                                title="Delete ${bus_manufacture.name}" 
	                                data-placement="bottom">
	                            <i class="fa fa-trash" aria-hidden="true"></i>
	                        </button>
                        </c:if>

                    </td>

                    <td>${bus_manufacture.name}</td>
                </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-bus-manufactures.jsp" />
