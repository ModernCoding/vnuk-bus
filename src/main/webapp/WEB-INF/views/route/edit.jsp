<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Update route n� ${route.id}</h3>
    <form action="../update" method="post">
        <h1>Add a new route</h1>
		<hr/>
		<input type="hidden" name="id" value="${route.id}" />
		
		<b>DepartureTerminal :</b> 
			<textarea name="departure_terminal" cols="50">${route.departure_terminal}</textarea>	<br/>
		<b>ArrivalTerminal :</b> 
			<textarea name="arrival_terminal" cols="50">${route.arrival_terminal}</textarea> <br/>
		<b>Information :</b> 
			<textarea name="information" cols="50">${route.information}</textarea> <br/>	
	
		<hr/>			
		<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../../routes">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close-routes.jsp" />
