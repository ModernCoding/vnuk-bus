<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Create route</h3>
    <form:errors path="busType.label" cssStyle="color: red;"/><br/>
    <form:errors path="busType.information" cssStyle="color: red;"/>
    <form action="create" method="post">
        <h1>Add a new route</h1>
		<hr/>
		<table class="table table-bordered table-hover table-responsive table-striped">
			<tbody>
				<tr>
					
                    <td>
                    	<input type="hidden" name="id" value="${route.id}" />
						DepartureTerminal: <input type="text" name="departure_terminal"/><br/>
					</td>
					
					<td>
					ArrivalTerminal: <input type="text" name="arrival_terminal"/><br/>
					</td>
					
					<td>
						Information: <input type="text" name="information"/><br/>
					</td>
										
				</tr>
			</tbody>
		</table>	
				<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../routes">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
