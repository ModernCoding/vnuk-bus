<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />

    <a href="route/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New route
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>DepartureTerminal</th>
                <th>ArrivalTerminal</th>
                <th>Information</th>
               
            </tr>
        </thead>

        <tbody>

            <c:forEach var="route" items="${routes}">
            
                <tr>
                    <td>                     
                        <a href="route/${route.id}"
	                        class="btn btn-xs btn-default" 
	                        data-toggle="tooltip" 
	                        title="Show ${route.departure_time}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-eye" aria-hidden="true"></i>
                        </a> 
                        
                        <a href="route/edit/${route.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${route.departure_time}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-pencil" aria-hidden="true"></i>
                        </a> 
                        
                        <button type="button"
                                class="btn btn-xs btn-danger my-bus-type-to-delete" 
                                value="${route.id}"
                                data-toggle="tooltip" 
                                title="Delete ${route.departure_time}" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>

                    <td>${route.departure_terminal}</td>
                    <td>${route.arrival_terminal}</td>
                    <td>${route.information}</td>
                  
                </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-routes.jsp" />
