<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>DepartureTerminal</th>
                <th>ArrivalTerminal</th>
                <th>Information</th>
                
            </tr>
        </thead>

        <tbody>

	        <tr>
	            <td>
	                <a href="edit/${route.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${route.departure_terminal}" 
	                        data-placement="bottom"
                        >
                    	<i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
	            </td>
	
	            <td>${route.departure_terminal}</td>
	            <td>${route.arrival_terminal}</td>
	            <td>${route.information}</td>
	           
	        </tr>

        </tbody>
    </table>
    <a class="btn btn-default" href="../routes">
	    Back
	</a>
            
<c:import url="/resources/jsp/body-close-routes.jsp" />
