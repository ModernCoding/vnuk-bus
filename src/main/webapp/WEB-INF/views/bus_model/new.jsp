<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Create bus model</h3>
    <form:errors path="bus_model.name" cssStyle="color: red;"/><br/>
    <form:errors path="bus_model.busManufactureId" cssStyle="color: red;"/>
    <form action="create" method="post">
        <h1>Add a new bus model</h1>
		<hr/>
		<table class="table table-bordered table-hover table-responsive table-striped">
			<tbody>
				<tr>
					
                    <td>
                    	<input type="hidden" name="id" value="${bus_model.id}" />
						Name: <input type="text" name="name"/><br/>
					</td>
					
					<td>
						Bus Manufacture: <br/>
						<form:select path = "bus_model.busManufactureId">
				        	<form:options items = "${busManufactureList}" />
				        </form:select>
					</td>

					
				</tr>
			</tbody>
		</table>	
				<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../bus_models">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
