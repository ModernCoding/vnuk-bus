<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Update bus model n� ${bus_model.id}</h3>
    <form action="../update" method="post">
        <h1>Add a new contact</h1>
		<hr/>
		<input type="hidden" name="id" value="${bus_model.id}" />
		
		<b>Name :</b> 
			<textarea name="name" cols="50">${bus_model.name}</textarea>	<br/>
		<b>Bus Manufacture ID :</b> 
			<textarea name="bus_manufacture_id" cols="50">${bus_model.bus_manufacture_id}</textarea> <br/>
		
		<hr/>			
		<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../../bus_models">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close-bus-models.jsp" />
