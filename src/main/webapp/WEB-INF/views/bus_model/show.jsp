<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Name</th>
                <th>Bus Manufacture ID</th>
            </tr>
        </thead>

        <tbody>

	        <tr>
	            <td>
	                <a href="edit/${bus_model.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${bus_model.name}" 
	                        data-placement="bottom"
                        >
                    	<i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
	                
	            </td>
	
	            <td>${bus_model.name}</td>
	            <td>${bus_model.country_id}</td>
	        </tr>

        </tbody>
    </table>
    <a class="btn btn-default" href="../bus_models">
	    Back
	</a>
            
<c:import url="/resources/jsp/body-close-bus-models.jsp" />
