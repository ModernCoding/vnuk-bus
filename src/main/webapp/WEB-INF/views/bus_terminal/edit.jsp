<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Update bus terminal n� ${bus_terminal.id}</h3>
    <form action="../update" method="post">
        <h1>Add a new bus terminal</h1>
		<hr/>
		<input type="hidden" name="id" value="${bus_terminal.id}" />
		
		<b>City Id :</b> 
			<textarea name="city_id" cols="50">${bus_type.city_id}</textarea>	<br/>
		<b>Information :</b> 
			<textarea name="information" cols="50">${bus_type.information}</textarea> <br/>
		<b>Bus Address :</b> 
			<textarea name="bus_address" cols="50">${bus_type.bus_address}</textarea>	<br/>
		<b>Phone Number :</b> 
			<textarea name="phone_number" cols="50">${bus_type.phone_number}</textarea>	<br/>
		<b>Photo Id :</b> 
			<textarea name="photo_id" cols="50">${bus_type.photo_id}</textarea>	<br/>			
	
		<hr/>			
		<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../../bus_terminals">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close-bus-types.jsp" />
