<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>City Id</th>
                <th>Information</th>
                <th>Bus Address</th>
                <th>Phone Number</th>
                <th>Photo Id</th>
                
            </tr>
        </thead>

        <tbody>

	        <tr>
	            <td>
	                <a href="edit/${bus_terminal.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${bus_terminal.city_id}" 
	                        data-placement="bottom"
                        >
                    	<i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
	            </td>
	
	            <td>${bus_terminal.city_id}</td>
	            <td>${bus_terminal.information}</td>
	            <td>${bus_terminal.bus_address}</td>
	            <td>${bus_terminal.phone_number}</td>
	            <td>${bus_terminal.photo_id}</td>
	           
	        </tr>

        </tbody>
    </table>
    <a class="btn btn-default" href="../bus_terminals">
	    Back
	</a>
            
<c:import url="/resources/jsp/body-close-bus-terminals.jsp" />
