<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />

    <a href="bus_terminal/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New bus terminal
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>City ID</th>
                <th>Information</th>
                <th>Bus Address</th>
                <th>Phone Number</th>
                <th>Photo ID</th>
               
            </tr>
        </thead>

        <tbody>

            <c:forEach var="bus_terminal" items="${bus_terminals}">
            
                <tr>
                    <td>                     
                        <a href="bus_terminal/${bus_terminal.id}"
	                        class="btn btn-xs btn-default" 
	                        data-toggle="tooltip" 
	                        title="Show ${bus_terminal.label}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-eye" aria-hidden="true"></i>
                        </a> 
                        
                        <a href="bus_terminal/edit/${bus_terminal.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${bus_terminal.label}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-pencil" aria-hidden="true"></i>
                        </a> 
                        
                        <button type="button"
                                class="btn btn-xs btn-danger my-bus-terminal-to-delete" 
                                value="${bus_terminal.id}"
                                data-toggle="tooltip" 
                                title="Delete ${bus_terminal.label}" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>

                    <td>${bus_type.city_id}</td>
                    <td>${bus_type.information}</td>
                    <td>${bus_type.bus_address}</td>
                    <td>${bus_type.phone_number}</td>
                    <td>${bus_type.photo_id}</td>
                   
                </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-bus-terminals.jsp" />
