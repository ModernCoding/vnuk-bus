<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Create bus terminal</h3>
    <form:errors path="busType.cityId" cssStyle="color: red;"/><br/>
    <form:errors path="busType.information" cssStyle="color: red;"/>
    <form:errors path="busType.busAddress" cssStyle="color: red;"/><br/>
    <form:errors path="busType.phoneNumber" cssStyle="color: red;"/><br/>
    <form:errors path="busType.photoId" cssStyle="color: red;"/><br/>
    
    <form action="create" method="post">
        <h1>Add a new bus terminal</h1>
		<hr/>
		<table class="table table-bordered table-hover table-responsive table-striped">
			<tbody>
				<tr>
					
                    <td>
                    	<input type="hidden" name="id" value="${bus_terminal.id}" />
						City Id: <input type="text" name="city_id"/><br/>
					</td>
					
					<td>
						Information: <input type="text" name="information"/><br/>
					</td>
					
					<td>
						Bus Address: <input type="text" name="bus_address"/><br/>
					</td>
					
					<td>
						Phone Number: <input type="text" name="phone_number"/><br/>
					</td>
					
					<td>
						Photo Id: <input type="text" name="photo_id"/><br/>
					</td>
										
				</tr>
			</tbody>
		</table>	
				<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../bus_terminals">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
