<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />

    <a href="ticket/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New Bus Manufacture
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Customer Id</th>
                <th>Phone Number</th>
                <th>Email</th>
                <th>Id card Number</th>
                <th>Route Id</th>
                <th>Price Id</th>
                <th>Bus Type Id</th>
                <th>Schedule Id</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="ticket" items="${tickets}">
            
                <tr>
                    <td>                     
                        <a href="ticket/${ticket.id}"
	                        class="btn btn-xs btn-default" 
	                        data-toggle="tooltip" 
	                        title="Show ${ticket.customer_id}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-eye" aria-hidden="true"></i>
                        </a> 
                        
                        <a href="ticket/edit/${ticket.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${ticket.customer_id}" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-pencil" aria-hidden="true"></i>
                        </a> 
                        
                        <button type="button"
                                class="btn btn-xs btn-danger my-ticket-to-delete" 
                                value="${ticket.id}"
                                data-toggle="tooltip" 
                                title="Delete ${ticket.customer_id}" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>

                    <td>${ticket.customer_id}</td>
                    <td>${ticket.phone_number}</td>
                    <td>${ticket.email}</td>
                    <td>${ticket.id_card_number}</td>
                    <td>${ticket.route_id}</td>
                    <td>${ticket.price_id}</td>
                    <td>${ticket.bus_type_id}</td>
                    <td>${ticket.schedule_id}</td>
                   
                </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-tickets.jsp" />
