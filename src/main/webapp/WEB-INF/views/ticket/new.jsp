<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Create Ticket</h3>
    <form:errors path="ticket.customer_id" cssStyle="color: red;"/>
    <form action="create" method="post">
        <h1>Add a new ticket</h1>
		<hr/>
		<table class="table table-bordered table-hover table-responsive table-striped">
			<tbody>
				<tr>
					
                    <td>
                    	<input type="hidden" name="id" value="${ticket.id}" />
						Customer ID: <input type="text" name="customer_id"/><br/>
					</td>
					
					 <td>
                    	
						Phone Number: <input type="text" name="phone_number"/><br/>
					</td>
					
					 <td>
						Email: <input type="text" name="email"/><br/>
					</td>
					
					 <td>
						ID Card Number: <input type="text" name="id_card_number"/><br/>
					</td>
					
					 <td>
						Route ID: <input type="text" name="route_id"/><br/>
					</td>
					 
					 <td>
						Price ID: <input type="text" name="price_id"/><br/>
					</td>
					
					 <td>
						Bus Type ID: <input type="text" name="bus_type_id"/><br/>
					</td>
					
					 <td>
						Schedule ID: <input type="text" name="schedule_id"/><br/>
					</td>
				</tr>
			</tbody>
		</table>	
				<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../tickets">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
