<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Update Ticket n� ${ticket.id}</h3>
    <form action="../update" method="post">
        <h1>Add a new ticket</h1>
		<hr/>
		<input type="hidden" name="id" value="${ticket.id}" />
		
		<b>Customer ID :</b> 
			<textarea name="name" cols="50">${ticket.customer_id}</textarea>	<br/>
		<b>Phone Number :</b> 
			<textarea name="name" cols="50">${ticket.phone_number}</textarea>	<br/>
		<b>Email :</b> 
			<textarea name="name" cols="50">${ticket.email}</textarea>	<br/>	
		<b>Id Card Number :</b> 
			<textarea name="name" cols="50">${ticket.id_card_number}</textarea>	<br/>	
		<b>Route Id :</b> 
			<textarea name="name" cols="50">${ticket.route_id}</textarea>
		<b>Price Id :</b> 
			<textarea name="name" cols="50">${ticket.price_id}</textarea>
		<b>Bus Type :</b> 
			<textarea name="name" cols="50">${ticket.bus_type_id}</textarea>
		<b>Schedule Id :</b> 
			<textarea name="name" cols="50">${ticket.schedule_id}</textarea>
			
		<hr/>
		<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../../tickets">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close-tickets.jsp" />
