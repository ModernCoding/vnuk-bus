<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>CustomerID</th>
                <th>PhoneNumber</th>
                <th>Email</th>
                <th>IDCardNumber</th>
                <th>RouteID</th>
                <th>PrinceID</th>
                <th>BusTypeID</th>
                <th>ScheduleID</th>
            </tr>
        </thead>

        <tbody>

	        <tr>
	            <td>
	                <a href="edit/${ticket.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit ${ticket.customer_id}" 
	                        data-placement="bottom"
                        >
                    	<i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>

	            </td>
	
	            <td>${schedule.route_id}</td>
                <td>${schedule.day_id}</td>
                <td>${schedule.departure_time}</td>
                <td>${schedule.arrival_time}</td>
	        </tr>

        </tbody>
    </table>
    <a class="btn btn-default" href="../schedules">
	    Back
	</a>
            
<c:import url="/resources/jsp/body-close-schedules.jsp" />
