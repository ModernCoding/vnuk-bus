<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:import url="/resources/jsp/body-open.jsp" />

    <a href="message/new" class="btn btn-primary">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;New message
    </a>

    <br /><br />
    
    <div id="my-notice"></div>
    
    <table class="table table-bordered table-hover table-responsive table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Messages</th>
                <th>Customer ID</th>
            </tr>
        </thead>

        <tbody>

            <c:forEach var="message" items="${messages}">
            
                <tr>
                    <td>                     
                        <a href="message/${message.id}"
	                        class="btn btn-xs btn-default" 
	                        data-toggle="tooltip" 
	                        title="Show message" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-eye" aria-hidden="true"></i>
                        </a> 
                        
                        <a href="message/edit/${message.id}"
                        	class="btn btn-xs btn-primary" 
	                        data-toggle="tooltip" 
	                        title="Edit message" 
	                        data-placement="bottom"
                        >
                        	<i class="fa fa-pencil" aria-hidden="true"></i>
                        </a> 
                        
                        <button type="button"
                                class="btn btn-xs btn-danger my-message-to-delete" 
                                value="${message.id}"
                                data-toggle="tooltip" 
                                title="Delete  message" 
                                data-placement="bottom">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>

                    </td>

                    <td>${message.messages}</td>
                    <td>${message.customer_id}</td>
                </tr>

            </c:forEach>

        </tbody>
    </table>
            
<c:import url="/resources/jsp/body-close-messages.jsp" />
