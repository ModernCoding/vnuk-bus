<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Update message n� ${message.id}</h3>
    <form action="../update" method="post">
        <h1>Add a new message</h1>
		<hr/>
		<input type="hidden" name="id" value="${message.id}" />
		
		<b>Message :</b> 
			<textarea name="messages" cols="50">${message.messages}</textarea>	<br/>
		<b>Customer id :</b> 
			<textarea name="customer_id" cols="50">${message.customer_id}</textarea> <br/>
	
		<hr/>			
		<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../../messages">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close-messages.jsp" />
