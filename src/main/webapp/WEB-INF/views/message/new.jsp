<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/body-open.jsp" />
    <h3>Create message</h3>
    <form:errors path="message.messages" cssStyle="color: red;"/><br/>
    <form:errors path="message.customerId" cssStyle="color: red;"/>
    <form action="create" method="post">
        <h1>Add a new message</h1>
		<hr/>
		<table class="table table-bordered table-hover table-responsive table-striped">
			<tbody>
				<tr>
					
                    <td>
                    	<input type="hidden" name="id" value="${message.id}" />
						Message: <input type="text" name="messages"/><br/>
					</td>
					
					<td>
						Customer ID: <input type="text" name="customer_id"/><br/>
					</td>

					
				</tr>
			</tbody>
		</table>	
				<input type="submit" value="Save" class="btn btn-success"/>

        <a class="btn btn-default" href="../messages">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
