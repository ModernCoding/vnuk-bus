$(function(){

    //  DELETING CUSTOMER
    $('.my-ticket-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var ticketId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "ticket/delete",
            
            data: {
                id: ticketId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('Ticket ' + ticketId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of ticket ' + ticketId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
