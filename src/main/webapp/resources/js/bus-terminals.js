$(function(){

    //  DELETING CUSTOMER
    $('.my-bus-terminal-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var busTerminalId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "bus_terminal/delete",
            
            data: {
                id: busTerminalId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('Bus Terminal ' + busTerminalId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of bus terminal ' + busTerminalId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
