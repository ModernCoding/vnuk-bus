$(function(){

    //  DELETING CUSTOMER
    $('.my-schedule-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var scheduleId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "schedule/delete",
            
            data: {
                id: scheduleId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('BusManufacture ' + scheduleId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of schedule ' + scheduleId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
