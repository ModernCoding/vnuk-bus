$(function(){

    //  DELETING CUSTOMER
    $('.my-city-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var cityId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "city/delete",
            
            data: {
                id: cityId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('City ' + cityId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of city ' + cityId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
