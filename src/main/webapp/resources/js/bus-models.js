$(function(){

    //  DELETING CUSTOMER
    $('.my-bus-model-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var bus_modelId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "bus_model/delete",
            
            data: {
                id: bus_modelrId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('BusModel ' + bus_modelId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of bus_model ' + bus_modelId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
