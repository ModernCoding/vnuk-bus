$(function(){

    //  DELETING CUSTOMER
    $('.my-price-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var priceId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "price/delete",
            
            data: {
                id: priceId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('Price ' + priceId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of bus type ' + priceId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
