$(function(){

    //  DELETING CUSTOMER
    $('.my-photo-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var photoId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "photo/delete",
            
            data: {
                id: photoId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('Photo ' + photoId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of photo ' + photoId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
