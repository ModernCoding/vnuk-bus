$(function(){

    //  DELETING CUSTOMER
    $('.my-customer-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var customerId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "customer/delete",
            
            data: {
                id: customerId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('Customer ' + customerId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of customer ' + customerId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
