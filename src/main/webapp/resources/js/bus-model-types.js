$(function(){

    //  DELETING CUSTOMER
    $('.my-bus-model-type-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var busModelTypeId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "bus_model_type/delete",
            
            data: {
                id: busModelTypeId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('BusModelType ' + bus_model_typeId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of bus_model_type ' + bus_model_typeId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
