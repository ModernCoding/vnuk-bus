$(function(){

    //  DELETING CUSTOMER
    $('.my-route-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var routeId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "route/delete",
            
            data: {
                id: routeId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('Route ' + routeId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of bus type ' + routeId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
