$(function(){

    //  DELETING CUSTOMER
    $('.my-season-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var seasonId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "season/delete",
            
            data: {
                id: seasonId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('Season ' + seasonId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of season ' + seasonId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
