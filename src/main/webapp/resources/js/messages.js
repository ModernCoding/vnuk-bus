$(function(){

    //  DELETING CUSTOMER
    $('.my-message-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var messageId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "message/delete",
            
            data: {
                id: messagerId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('Message ' + messageId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of message ' + messageId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
