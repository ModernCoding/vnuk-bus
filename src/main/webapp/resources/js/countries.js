$(function(){

    //  DELETING CUSTOMER
    $('.my-country-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var countryId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "country/delete",
            
            data: {
                id: countryId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('Country ' + countryId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of country ' + countryId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
