$(function(){

    //  DELETING CUSTOMER
    $('.my-bus-type-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var busTypeId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "bus_type/delete",
            
            data: {
                id: busTypeId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('Bus Type ' + busTypeId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of bus type ' + busTypeId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
