$(function(){

    //  DELETING CUSTOMER
    $('.my-day-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var dayId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "day/delete",
            
            data: {
                id: dayId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('Day ' + dayId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of day ' + dayId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
