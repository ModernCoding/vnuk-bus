$(function(){

    //  DELETING CUSTOMER
    $('.my-bus-manufacture-to-delete').on('click', function (e){
        e.preventDefault();

        var $this = $(this);
        var busManufactureId = $this.val();

        
        $.ajax({
           
            type: "POST",
            url: "bus_manufacture/delete",
            
            data: {
                id: busManufactureId
            },
            
            success: function() {
                
            	$this.closest('tr').remove();
                
                $('#my-notice').text('BusManufacture ' + busManufactureId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of bus_manufacture ' + busManufactureId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
