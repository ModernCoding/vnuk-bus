package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.edu.vnuk.rediebus.dao.PriceDao;
import vn.edu.vnuk.rediebus.model.Price;

@Controller
public class PricesController {
	private final PriceDao dao;
	
	@Autowired
	public PricesController(PriceDao dao) {
		this.dao = dao;
	}
    
	@RequestMapping("prices")
    public String read(Model model) throws SQLException{
        model.addAttribute("prices", dao.read());
        return "price/index";
    }
	
	@RequestMapping("price/new")
    public String add() {
		return ("price/new");
	}

    @RequestMapping("price/create")
    public String create(@Valid Price price, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("route_id") || result.hasFieldErrors("season_id") || result.hasFieldErrors("price")) {
            return "price/new";
        }
        
        dao.create(price);
        return "redirect:/prices";
    }
    
    
    @RequestMapping("price/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("price", dao.read(id));
        return "price/edit";
    }
    
    
    @RequestMapping("price/update")
    public String update(@Valid Price price, BindingResult result, Model model) throws SQLException{
    	dao.update(price);
        return "redirect:/prices";
    }
    
    @RequestMapping("price/{id}")
    public String show(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("price", dao.read(id));
        return "price/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="price/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
}
