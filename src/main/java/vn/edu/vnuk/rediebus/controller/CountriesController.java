package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.edu.vnuk.rediebus.dao.CountryDao;
import vn.edu.vnuk.rediebus.model.Country;

@Controller
public class CountriesController {
	private final CountryDao dao;
	
	@Autowired
	public CountriesController(CountryDao dao) {
		this.dao = dao;
	}
    
	@RequestMapping("countries")
    public String read(Model model) throws SQLException{
        model.addAttribute("countries", dao.read());
        return "country/index";
    }
	
	@RequestMapping("country/new")
    public String add() {
		return ("country/new");
	}

    @RequestMapping("country/create")
    public String create(@Valid Country country, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("name")) {
            return "country/new";
        }
        
        dao.create(country);
        return "redirect:/countries";
    }
    
    
    @RequestMapping("country/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("country", dao.read(id));
        return "country/edit";
    }
    
    
    @RequestMapping("country/update")
    public String update(@Valid Country country, BindingResult result, Model model) throws SQLException{
    	dao.update(country);
        return "redirect:/countries";
    }
    
    @RequestMapping("country/{id}")
    public String show(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("country", dao.read(id));
        return "country/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="country/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
}