package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.edu.vnuk.rediebus.dao.BusManufactureDao;
import vn.edu.vnuk.rediebus.model.BusManufacture;

@Controller
public class BusManufacturesController {
	private final BusManufactureDao dao;
	
	@Autowired
	public BusManufacturesController(BusManufactureDao dao) {
		this.dao = dao;
	}
    
	@RequestMapping("bus_manufactures")
    public String read(Model model) throws SQLException{
        model.addAttribute("bus_manufactures", dao.read());
        return "bus_manufacture/index";
    }
	
	@RequestMapping("bus_manufacture/new")
    public String add() {
		return ("bus_manufacture/new");
	}

    @RequestMapping("bus_manufacture/create")
    public String create(@Valid BusManufacture bus_manufacture, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("name")) {
            return "bus_manufacture/new";
        }
        
        dao.create(bus_manufacture);
        return "redirect:/bus_manufactures";
    }
    
    
    @RequestMapping("bus_manufacture/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("bus_manufacture", dao.read(id));
        return "bus_manufacture/edit";
    }
    
    
    @RequestMapping("bus_manufacture/update")
    public String update(@Valid BusManufacture bus_manufacture, BindingResult result, Model model) throws SQLException{
    	dao.update(bus_manufacture);
        return "redirect:/bus_manufactures";
    }
    
    @RequestMapping("bus_manufacture/{id}")
    public String show(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("bus_manufacture", dao.read(id));
        return "bus_manufacture/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="bus_manufacture/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
}
