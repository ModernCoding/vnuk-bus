package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.edu.vnuk.rediebus.dao.MessageDao;
import vn.edu.vnuk.rediebus.model.Message;

@Controller
public class MessagesController {
	private final MessageDao dao;
	
	@Autowired
	public MessagesController(MessageDao dao) {
		this.dao = dao;
	}
    
	@RequestMapping("messages")
    public String read(Model model) throws SQLException{
        model.addAttribute("messages", dao.read());
        return "message/index";
    }
	
	@RequestMapping("message/new")
    public String add() {
		return ("message/new");
	}

    @RequestMapping("message/create")
    public String create(@Valid Message message, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("message") || result.hasFieldErrors("customerId")) {
            return "message/new";
        }
        dao.create(message);
        return "redirect:/messages";
    }
    
    
    @RequestMapping("message/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("message", dao.read(id));
        return "message/edit";
    }
    
    
    @RequestMapping("message/update")
    public String update(@Valid Message message, BindingResult result, Model model) throws SQLException{
    	dao.update(message);
        return "redirect:/messages";
    }
    
    @RequestMapping("message/{id}")
    public String show(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("message", dao.read(id));
        return "message/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="message/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
}
