package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.edu.vnuk.rediebus.dao.ScheduleDao;
import vn.edu.vnuk.rediebus.model.Schedule;

@Controller
public class SchedulesController {
	private final ScheduleDao dao;
	
	@Autowired
	public SchedulesController(ScheduleDao dao) {
		this.dao = dao;
	}
    
	@RequestMapping("schedules")
    public String read(Model model) throws SQLException{
        model.addAttribute("schedules", dao.read());
        return "schedule/index";
    }
	
	@RequestMapping("schedule/new")
    public String add() {
		return ("schedule/new");
	}

    @RequestMapping("schedule/create")
    public String create(@Valid Schedule schedule, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("route_id") || result.hasFieldErrors("day_id") || result.hasFieldErrors("departure_time") || result.hasFieldErrors("arrival_time")) {
            return "schedule/new";
        }
        
        dao.create(schedule);
        return "redirect:/schedules";
    }
    
    
    @RequestMapping("schedule/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("schedule", dao.read(id));
        return "schedule/edit";
    }
    
    
    @RequestMapping("schedule/update")
    public String update(@Valid Schedule schedule, BindingResult result, Model model) throws SQLException{
    	dao.update(schedule);
        return "redirect:/schedules";
    }
    
    @RequestMapping("schedule/{id}")
    public String show(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("schedule", dao.read(id));
        return "schedule/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="schedule/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
}