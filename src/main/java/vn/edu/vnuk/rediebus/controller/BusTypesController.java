package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.edu.vnuk.rediebus.dao.BusTypeDao;
import vn.edu.vnuk.rediebus.model.BusType;

@Controller
public class BusTypesController {
	private final BusTypeDao dao;
	
	@Autowired
	public BusTypesController(BusTypeDao dao) {
		this.dao = dao;
	}
    
	@RequestMapping("bus_types")
    public String read(Model model) throws SQLException{
        model.addAttribute("bus_types", dao.read());
        return "bus_type/index";
    }
	
	@RequestMapping("bus_type/new")
    public String add() {
		return ("bus_type/new");
	}

    @RequestMapping("bus_type/create")
    public String create(@Valid BusType bus_type, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("label") || result.hasFieldErrors("information")) {
            return "bus_type/new";
        }
        
        dao.create(bus_type);
        return "redirect:/bus_types";
    }
    
    
    @RequestMapping("bus_type/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("bus_type", dao.read(id));
        return "bus_type/edit";
    }
    
    
    @RequestMapping("bus_type/update")
    public String update(@Valid BusType bus_type, BindingResult result, Model model) throws SQLException{
    	dao.update(bus_type);
        return "redirect:/bus_types";
    }
    
    @RequestMapping("bus_type/{id}")
    public String show(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("bus_type", dao.read(id));
        return "bus_type/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="bus_type/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
}
