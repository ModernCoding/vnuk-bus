package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.edu.vnuk.rediebus.dao.BusTerminalDao;
import vn.edu.vnuk.rediebus.model.BusTerminal;

@Controller
public class BusTerminalsController {
	private final BusTerminalDao dao;
	
	@Autowired
	public BusTerminalsController(BusTerminalDao dao) {
		this.dao = dao;
	}
    
	@RequestMapping("bus_terminals")
    public String read(Model model) throws SQLException{
        model.addAttribute("bus_terminals", dao.read());
        return "bus_terminal/index";
    }
	
	@RequestMapping("bus_terminal/new")
    public String add() {
		return ("bus_terminal/new");
	}

    @RequestMapping("bus_terminal/create")
    public String create(@Valid BusTerminal bus_terminal, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("city_id") || result.hasFieldErrors("information") || result.hasFieldErrors("bus_address") || result.hasFieldErrors("phone_number") || result.hasFieldErrors("photo_id") ) {
            return "bus_terminal/new";
        }
        
        dao.create(bus_terminal);
        return "redirect:/bus_terminals";
    }
    
    
    @RequestMapping("bus_terminal/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("bus_terminal", dao.read(id));
        return "bus_terminal/edit";
    }
    
    
    @RequestMapping("bus_terminal/update")
    public String update(@Valid BusTerminal bus_terminal, BindingResult result, Model model) throws SQLException{
    	dao.update(bus_terminal);
        return "redirect:/bus_terminals";
    }
    
    @RequestMapping("bus_terminal/{id}")
    public String show(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("bus_terminal", dao.read(id));
        return "bus_terminal/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="bus_terminal/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
}
