package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.edu.vnuk.rediebus.dao.DayDao;
import vn.edu.vnuk.rediebus.model.Day;

@Controller
public class DaysController {
	private final DayDao dao;
	
	@Autowired
	public DaysController(DayDao dao) {
		this.dao = dao;
	}
    
	@RequestMapping("days")
    public String read(Model model) throws SQLException{
        model.addAttribute("days", dao.read());
        return "day/index";
    }
	
	@RequestMapping("day/new")
    public String add() {
		return ("day/new");
	}

    @RequestMapping("day/create")
    public String create(@Valid Day day, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("name")) {
            return "day/new";
        }
        
        dao.create(day);
        return "redirect:/days";
    }
    
    
    @RequestMapping("day/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("day", dao.read(id));
        return "day/edit";
    }
    
    
    @RequestMapping("day/update")
    public String update(@Valid Day day, BindingResult result, Model model) throws SQLException{
    	dao.update(day);
        return "redirect:/days";
    }
    
    @RequestMapping("day/{id}")
    public String show(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("day", dao.read(id));
        return "day/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="day/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
}
