package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.edu.vnuk.rediebus.dao.CustomerDao;
import vn.edu.vnuk.rediebus.model.Customer;

@Controller
public class CustomersController {
	private final CustomerDao dao;
	
	@Autowired
	public CustomersController(CustomerDao dao) {
		this.dao = dao;
	}
    
	@RequestMapping("customers")
    public String read(Model model) throws SQLException{
        model.addAttribute("customers", dao.read());
        return "customer/index";
    }
	
	@RequestMapping("customer/new")
    public String add() {
		return ("customer/new");
	}

    @RequestMapping("customer/create")
    public String create(@Valid Customer customer, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("name") || result.hasFieldErrors("email") || result.hasFieldErrors("phone") ) {
            return "customer/new";
        }
        
        dao.create(customer);
        return "redirect:/customers";
    }
    
    
    @RequestMapping("customer/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("customer", dao.read(id));
        return "customer/edit";
    }
    
    
    @RequestMapping("customer/update")
    public String update(@Valid Customer customer, BindingResult result, Model model) throws SQLException{
    	dao.update(customer);
        return "redirect:/customers";
    }
    
    @RequestMapping("customer/{id}")
    public String show(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("customer", dao.read(id));
        return "customer/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="customer/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
}
