package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.rediebus.dao.CityDao;
import vn.edu.vnuk.rediebus.dao.CountryDao;
import vn.edu.vnuk.rediebus.model.City;
import vn.edu.vnuk.rediebus.model.Country;

@Controller
public class CitiesController {
	private final CityDao cityDao;
	private final CountryDao countryDao;
	
	@Autowired
	public CitiesController(CityDao cityDao, CountryDao countryDao) {
		this.cityDao = cityDao;
		this.countryDao = countryDao;
	}
    
	@RequestMapping("cities")
    public String read(Model model) throws SQLException{
        model.addAttribute("cities", cityDao.read());
        return "city/index";
	}
	
	@RequestMapping("city/new")
    public String add(Model model){
		 model.addAttribute("city", new City());
	     return "city/new";
	}

    @RequestMapping("city/create")
    public String create(@Valid City city, BindingResult result) throws SQLException{
        
        if (result.hasFieldErrors("name") || result.hasFieldErrors("country_id")) {
            return "city/new";
        }
        cityDao.create(city);
        return "redirect:/cities";
    }
    
    
    @RequestMapping("city/edit/{id}")
    public String edit(@PathVariable("id") @RequestParam Map<String, String> cityId, Model model) throws SQLException{
    	int id = Integer.parseInt(cityId.toString());
        model.addAttribute("city", cityDao.read(id));
        return "city/edit";
    }
    
    
    @RequestMapping("city/update")
    public String update(@Valid City city, BindingResult result) throws SQLException{
    	cityDao.update(city);
        return "redirect:/cities";
    }
    
    @RequestMapping("city/{id}")
    public String show(@PathVariable("id") @RequestParam Map<String, String> cityId, Model model) throws SQLException{
    	int id = Integer.parseInt(cityId.toString());
        model.addAttribute("city", cityDao.read(id));
        return "city/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="city/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
    	cityDao.delete(id);
        response.setStatus(200);
    }
    
    
    @ModelAttribute("countryList")
    public Map<Long, String> getCountries(){
    	Map<Long, String> countryList = new HashMap<Long, String>();
	     
		 try {
			 for(Country country : countryDao.read()) {
				 countryList.put(country.getId(), country.getName());
			 }
			 
		 } catch (SQLException e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
		 }
		 
		 return countryList;
    }
}
