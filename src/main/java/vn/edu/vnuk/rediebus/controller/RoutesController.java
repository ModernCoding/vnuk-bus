package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.edu.vnuk.rediebus.dao.RouteDao;
import vn.edu.vnuk.rediebus.model.Route;

@Controller
public class RoutesController {
	private final RouteDao dao;
	
	@Autowired
	public RoutesController(RouteDao dao) {
		this.dao = dao;
	}
    
	@RequestMapping("routes")
    public String read(Model model) throws SQLException{
        model.addAttribute("route", dao.read());
        return "route/index";
    }
	
	@RequestMapping("route/new")
    public String add() {
		return ("route/new");
	}

    @RequestMapping("route/create")
    public String create(@Valid Route route, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("departure_terminal") || result.hasFieldErrors("arrival_terminal") || result.hasFieldErrors("information")) {
            return "route/new";
        }
        
        dao.create(route);
        return "redirect:/route";
    }
    
    
    @RequestMapping("route/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("route", dao.read(id));
        return "route/edit";
    }
    
    
    @RequestMapping("route/update")
    public String update(@Valid Route route, BindingResult result, Model model) throws SQLException{
    	dao.update(route);
        return "redirect:/route";
    }
    
    @RequestMapping("route/{id}")
    public String show(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("route", dao.read(id));
        return "route/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="route/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
}
