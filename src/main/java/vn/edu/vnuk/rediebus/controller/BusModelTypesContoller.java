package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.rediebus.dao.BusModelDao;
import vn.edu.vnuk.rediebus.dao.BusModelTypeDao;
import vn.edu.vnuk.rediebus.dao.BusTypeDao;
import vn.edu.vnuk.rediebus.dao.PhotoDao;
import vn.edu.vnuk.rediebus.model.BusModel;
import vn.edu.vnuk.rediebus.model.BusModelType;
import vn.edu.vnuk.rediebus.model.BusType;
import vn.edu.vnuk.rediebus.model.Photo;

@Controller
public class BusModelTypesContoller {
	private final BusModelTypeDao busmodeltypedao;
	private final BusTypeDao bustypeDao;
	private final BusModelDao busmodelDao;
	private final PhotoDao photoDao;
	
	@Autowired
	public BusModelTypesContoller(BusModelTypeDao busmodeltypedao,BusTypeDao bustypeDao,BusModelDao busmodelDao, PhotoDao photoDao) {
		this.busmodeltypedao = busmodeltypedao;
		this.bustypeDao = bustypeDao;
		this.busmodelDao = busmodelDao;
		this.photoDao = photoDao;
	}
    
	@RequestMapping("bus_model_types")
    public String read(Model model) throws SQLException{
        model.addAttribute("bus_model_types", busmodeltypedao.read());
        return "bus_model_type/index";
    }
	
	@RequestMapping("bus_model_type/new")
	public String add(Model model) {
		model.addAttribute("bus_model_type", new BusModelType());
        return ("bus_model_type/new");
	}

    @RequestMapping("bus_model_type/create")
    public String create(@Valid BusModelType bus_model_type, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("bus_type_id") || result.hasFieldErrors("bus_model_id") ||result.hasFieldErrors("photo_id")) {
            return "bus_model_type/new";
        }
        
        busmodeltypedao.create(bus_model_type);
        return "redirect:/bus_model_types";
    }
    
    
    @RequestMapping("bus_model_type/edit/{id}")
    public String edit(@PathVariable("id") @RequestParam Map<String, String> busModelTypeId, Model model) throws SQLException{
    	int id = Integer.parseInt(busModelTypeId.toString());
    	model.addAttribute("bus_model_type", busmodeltypedao.read(id));
        return "bus_model_type/edit";
    }
    
    
    @RequestMapping("bus_model_type/update")
    public String update(@Valid BusModelType bus_model_type, BindingResult result, Model model) throws SQLException{
    	busmodeltypedao.update(bus_model_type);
        return "redirect:/bus_model_types";
    }
    
    @RequestMapping("bus_model_type/{id}")
    public String show(@PathVariable("id") @RequestParam Map<String, String> busModelTypeId, Model model) throws SQLException{
    	int id = Integer.parseInt(busModelTypeId.toString());
    	model.addAttribute("bus_model_type", busmodeltypedao.read(id));
        return "bus_model_type/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="bus_model_type/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
    	busmodeltypedao.delete(id);
        response.setStatus(200);
    }
    
    @ModelAttribute("busTypeList")
    public Map<Long, String> getBusTypes(){
    	Map<Long, String> busTypeList = new HashMap<Long, String>();
	     
		 try {
			 for(BusType bus_type : bustypeDao.read()) {
				 busTypeList.put(bus_type.getId(), bus_type.getLabel());
			 }
			 
		 } catch (SQLException e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
		 }
		 
		 return busTypeList;
    }
    @ModelAttribute("busModelList")
    public Map<Long, String> getBusModels(){
    	Map<Long, String> busModelList = new HashMap<Long, String>();
	     
		 try {
			 for(BusModel bus_model : busmodelDao.read()) {
				 busModelList.put(bus_model.getId(), bus_model.getName());
			 }
			 
		 } catch (SQLException e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
		 }
		 
		 return busModelList;
    }
    @ModelAttribute("photoList")
    public Map<Long, String> getPhotos(){
    	Map<Long, String> photoList = new HashMap<Long, String>();
	     
		 try {
			 for(Photo photo : photoDao.read()) {
				 photoList.put(photo.getId(), photo.getLink());
			 }
			 
		 } catch (SQLException e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
		 }
		 
		 return photoList;
    }
}
