package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.edu.vnuk.rediebus.dao.TicketDao;
import vn.edu.vnuk.rediebus.model.Ticket;

@Controller
public class TicketsController {
	private final TicketDao dao;
	
	@Autowired
	public TicketsController(TicketDao dao) {
		this.dao = dao;
	}
    
	@RequestMapping("tickets")
    public String read(Model model) throws SQLException{
        model.addAttribute("tickets", dao.read());
        return "ticket/index";
    }
	
	@RequestMapping("ticket/new")
    public String add() {
		return ("ticket/new");
	}

    @RequestMapping("ticket/create")
    public String create(@Valid Ticket ticket, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("customer_id") || result.hasFieldErrors("phone_number") || result.hasFieldErrors("email") || result.hasFieldErrors("id_card_number") || result.hasFieldErrors("route_id") || result.hasFieldErrors("price_id") || result.hasFieldErrors("bus_type_id") || result.hasFieldErrors("schedule_id")) {
            return "ticket/new";
        }
        
        dao.create(ticket);
        return "redirect:/tickets";
    }
    
    
    @RequestMapping("ticket/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("ticket", dao.read(id));
        return "ticket/edit";
    }
    
    
    @RequestMapping("ticket/update")
    public String update(@Valid Ticket ticket, BindingResult result, Model model) throws SQLException{
    	dao.update(ticket);
        return "redirect:/tickets";
    }
    
    @RequestMapping("ticket/{id}")
    public String show(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("ticket", dao.read(id));
        return "ticket/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="ticket/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
}