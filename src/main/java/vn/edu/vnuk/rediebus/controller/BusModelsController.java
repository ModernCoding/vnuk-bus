package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.rediebus.dao.BusModelDao;
import vn.edu.vnuk.rediebus.dao.BusManufactureDao;
import vn.edu.vnuk.rediebus.model.BusManufacture;
import vn.edu.vnuk.rediebus.model.BusModel;


@Controller
public class BusModelsController {
	private final BusModelDao busModeldao;
	private final BusManufactureDao busManufactureDao;
	
	@Autowired
	public BusModelsController(BusModelDao busModeldao, BusManufactureDao busManufactureDao) {
		this.busModeldao = busModeldao;
		this.busManufactureDao = busManufactureDao;
	}
    
	@RequestMapping("bus_models")
    public String read(Model model) throws SQLException{
        model.addAttribute("bus_models", busModeldao.read());
        return "bus_model/index";
    }
	
	@RequestMapping("bus_model/new")
    public String add(Model model) throws SQLException{
		model.addAttribute("bus_model", new BusModel());
		return ("bus_model/new");
	}

    @RequestMapping("bus_model/create")
    public String create(@Valid BusModel bus_model, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("name")) {
            return "bus_model/new";
        }
        busModeldao.create(bus_model);
        return "redirect:/bus_models";
    }
    
    
    @RequestMapping("bus_model/edit/{id}")
    public String edit(@PathVariable("id") @RequestParam Map<String, String> busModelId, Model model) throws SQLException{
    	int id = Integer.parseInt(busModelId.toString());
    	model.addAttribute("bus_model", busModeldao.read(id));
        return "bus_model/edit";
    }
    
    
    @RequestMapping("bus_model/update")
    public String update(@Valid BusModel bus_model, BindingResult result, Model model) throws SQLException{
    	busModeldao.update(bus_model);
        return "redirect:/bus_models";
    }
    
    @RequestMapping("bus_model/{id}")
    public String show(@PathVariable("id") @RequestParam Map<String, String> busModelId, Model model) throws SQLException{
    	int id = Integer.parseInt(busModelId.toString());
    	model.addAttribute("bus_model", busModeldao.read(id));
        return "bus_model/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="bus_model/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
    	busModeldao.delete(id);
        response.setStatus(200);
    }
    
    @ModelAttribute("busManufactureList")
    public Map<Long, String> getbus_manufactures(){
    	Map<Long, String> busManufactureList = new HashMap<Long, String>();
	     
		 try {
			 for(BusManufacture bus_manufacture : busManufactureDao.read()) {
				 busManufactureList.put(bus_manufacture.getId(), bus_manufacture.getName());
			 }
			 
		 } catch (SQLException e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
		 }
		 
		 return busManufactureList;
    }
}
