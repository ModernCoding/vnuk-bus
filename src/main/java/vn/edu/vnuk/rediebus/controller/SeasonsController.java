package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.edu.vnuk.rediebus.dao.SeasonDao;
import vn.edu.vnuk.rediebus.model.Season;

@Controller
public class SeasonsController {
	private final SeasonDao dao;
	
	@Autowired
	public SeasonsController(SeasonDao dao) {
		this.dao = dao;
	}
    
	@RequestMapping("seasons")
    public String read(Model model) throws SQLException{
        model.addAttribute("seasons", dao.read());
        return "season/index";
    }
	
	@RequestMapping("season/new")
    public String add() {
		return ("season/new");
	}

    @RequestMapping("season/create")
    public String create(@Valid Season season, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("label") || result.hasFieldErrors("information")) {
            return "season/new";
        }
        
        dao.create(season);
        return "redirect:/seasons";
    }
    
    
    @RequestMapping("season/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("season", dao.read(id));
        return "season/edit";
    }
    
    
    @RequestMapping("season/update")
    public String update(@Valid Season season, BindingResult result, Model model) throws SQLException{
    	dao.update(season);
        return "redirect:/seasons";
    }
    
    @RequestMapping("season/{id}")
    public String show(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("season", dao.read(id));
        return "season/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="season/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
}