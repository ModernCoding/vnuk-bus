package vn.edu.vnuk.rediebus.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.edu.vnuk.rediebus.dao.PhotoDao;
import vn.edu.vnuk.rediebus.model.Photo;

@Controller
public class PhotosController {
	private final PhotoDao dao;
	
	@Autowired
	public PhotosController(PhotoDao dao) {
		this.dao = dao;
	}
    
	@RequestMapping("photos")
    public String read(Model model) throws SQLException{
        model.addAttribute("photos", dao.read());
        return "photo/index";
    }
	
	@RequestMapping("photo/new")
    public String add() {
		return ("photo/new");
	}

    @RequestMapping("photo/create")
    public String create(@Valid Photo photo, BindingResult result, Model model) throws SQLException{
        
        if (result.hasFieldErrors("link")) {
            return "photo/new";
        }
        
        dao.create(photo);
        return "redirect:/photos";
    }
    
    
    @RequestMapping("photo/edit/{id}")
    public String edit(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("photo", dao.read(id));
        return "photo/edit";
    }
    
    
    @RequestMapping("photo/update")
    public String update(@Valid Photo photo, BindingResult result, Model model) throws SQLException{
    	dao.update(photo);
        return "redirect:/photos";
    }
    
    @RequestMapping("photo/{id}")
    public String show(@PathVariable("id") int id, Model model) throws SQLException{
        model.addAttribute("photo", dao.read(id));
        return "photo/show";
    }
    
    
    //  DELETE WITH AJAX
    @RequestMapping(value="photo/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        dao.delete(id);
        response.setStatus(200);
    }
}
