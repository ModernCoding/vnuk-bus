package vn.edu.vnuk.rediebus.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Season {
	private Long id;
    
	@NotNull
    @Size(min = 5, message="Label must contain at least 5 characters")
	private String label;
	
    @NotNull
    @Size(min = 5, message="Information must be valid")
    private String information;
    private Integer numberOfPrices;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public Integer getNumberOfPrices() {
		return numberOfPrices;
	}

	public void setNumberOfPrices(Integer numberOfPrices) {
		this.numberOfPrices = numberOfPrices;
	}
	
}
