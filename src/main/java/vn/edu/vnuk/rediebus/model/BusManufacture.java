package vn.edu.vnuk.rediebus.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class BusManufacture {

	 	private Long id ;
	 	
	 	@NotNull
	    @Size(min = 5, message="Name must contain at least 5 characters")
	 	private String name;
	 	private Integer numberOfBusModels;
	 	
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Integer getNumberOfBusModels() {
			return numberOfBusModels;
		}
		public void setNumberOfBusModels(Integer numberOfBusModels) {
			this.numberOfBusModels = numberOfBusModels;
		}
		
		
		
}