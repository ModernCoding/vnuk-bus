package vn.edu.vnuk.rediebus.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Country {
	private Long id;
	
	@NotNull
    @Size(min = 3, message="Name must contain at least 3 characters")
	private String name;
	private Integer numberOfCities;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getNumberOfCities() {
		return numberOfCities;
	}
	public void setNumberOfCities(Integer numberOfCities) {
		this.numberOfCities = numberOfCities;
	}

}
