package vn.edu.vnuk.rediebus.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class BusModel {
	private Long id;
	
	@NotNull
    @Size(min = 5, message="Name must contain at least 5 characters")
	private String name;
	
	@NotNull(message="bus manufacture id must be correct")
	private Long busManufactureId;
	
	private BusManufacture bus_manufacture;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Long getBusManufactureId() {
		return busManufactureId;
	}
	
	public void setBusManufactureId(Long busManufactureId) {
		this.busManufactureId = busManufactureId;
	}
	
	public BusManufacture getBus_manufacture() {
		return bus_manufacture;
	}
	
	public void setBus_manufacture(BusManufacture bus_manufacture) {
		this.bus_manufacture = bus_manufacture;
	}
	
}
