package vn.edu.vnuk.rediebus.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Customer {
	private Long id ;
	
	@NotNull
    @Size(min = 2, message="Name must contain at least 2 characters")
	private String name;
	
	@NotNull
    @Size(min = 5, message="Email must be valid")
	private String email;

	@NotNull
    @Size(min = 5, message="Phone must be valid")
	private String phone;
	
	private Integer numberOfTickets;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getNumberOfTickets() {
		return numberOfTickets;
	}

	public void setNumberOfTickets(Integer numberOfTickets) {
		this.numberOfTickets = numberOfTickets;
	}
	
}
