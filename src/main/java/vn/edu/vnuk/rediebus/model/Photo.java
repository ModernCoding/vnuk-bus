package vn.edu.vnuk.rediebus.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Photo {
	
	 	private Long id ; 
	 	@NotNull
	    @Size(min = 5, message="Link must be valid")
	 	private String link;
	 	
	 	private Integer numberOfBusModelTypes;
	 	private Integer numberOfBusTerminals;
		
		public Photo() {};
	 	public Photo(Long id, String link) {
	 		this.id = id;
	 		this.link = link;

}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getLink() {
			return link;
		}
		public void setLink(String link) {
			this.link = link;
		}
		public Integer getNumberOfBusModelTypes() {
			return numberOfBusModelTypes;
		}
		public void setNumberOfBusModelTypes(Integer numberOfBusModelTypes) {
			this.numberOfBusModelTypes = numberOfBusModelTypes;
		}
		public Integer getNumberOfBusTerminals() {
			return numberOfBusTerminals;
		}
		public void setNumberOfBusTerminals(Integer numberOfBusTerminals) {
			this.numberOfBusTerminals = numberOfBusTerminals;
		}

}