package vn.edu.vnuk.rediebus.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Route {
	private Long id;
    
	private Long departureTerminal;
	private Long arrivalTerminal;
	
    @NotNull
    @Size(min = 5, message="Information must contain at least 5 characters")
    private String information;
    
    private Integer numberOfTickets;
    private BusTerminal depterminal;
    private BusTerminal arrterminal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDepartureTerminal() {
		return departureTerminal;
	}

	public void setDepartureTerminal(Long departureTerminal) {
		this.departureTerminal = departureTerminal;
	}

	public Long getArrivalTerminal() {
		return arrivalTerminal;
	}

	public void setArrivalTerminal(Long arrivalTerminal) {
		this.arrivalTerminal = arrivalTerminal;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public Integer getNumberOfTickets() {
		return numberOfTickets;
	}

	public void setNumberOfTickets(Integer numberOfTickets) {
		this.numberOfTickets = numberOfTickets;
	}

	public BusTerminal getDepterminal() {
		return depterminal;
	}

	public void setDepterminal(BusTerminal depterminal) {
		this.depterminal = depterminal;
	}

	public BusTerminal getArrterminal() {
		return arrterminal;
	}

	public void setArrterminal(BusTerminal arrterminal) {
		this.arrterminal = arrterminal;
	}

	
    
    
}
