package vn.edu.vnuk.rediebus.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Message {

	 	private Long id  ;
	 	@NotNull
	    @Size(min = 5, message="message must contain at least 5 characters")
	 	private String messages;
	 	@NotNull(message="Customer ID must be correct")
	 	private Long customer_id;

		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getMessages() {
			return messages;
		}
		public void setMessages(String messages) {
			this.messages = messages;
		}
		public Long getCustomer_id() {
			return customer_id;
		}
		public void setCustomer_id(Long customer_id) {
			this.customer_id = customer_id;
		}
		
}