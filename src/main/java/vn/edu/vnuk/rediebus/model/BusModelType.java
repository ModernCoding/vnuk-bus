package vn.edu.vnuk.rediebus.model;

import javax.validation.constraints.NotNull;

public class BusModelType {
	private Long id;
	@NotNull(message="Bus Type ID must be correct")
	private Long busTypeId;
	@NotNull(message="Bus Model ID must be correct")
	private Long busModelId;
	@NotNull(message="Photo ID must be correct")
	private Long photoId;
	
	private Photo photo;
	private BusModel bus_model;
	private BusType bus_type;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getBusTypeId() {
		return busTypeId;
	}
	public void setBusTypeId(Long busTypeId) {
		this.busTypeId = busTypeId;
	}
	public Long getBusModelId() {
		return busModelId;
	}
	public void setBusModelId(Long busModelId) {
		this.busModelId = busModelId;
	}
	public Long getPhotoId() {
		return photoId;
	}
	public void setPhotoId(Long photoId) {
		this.photoId = photoId;
	}
	public Photo getPhoto() {
		return photo;
	}
	public void setPhoto(Photo photo) {
		this.photo = photo;
	}
	public BusModel getBus_model() {
		return bus_model;
	}
	public void setBus_model(BusModel bus_model) {
		this.bus_model = bus_model;
	}
	public BusType getBus_type() {
		return bus_type;
	}
	public void setBus_type(BusType bus_type) {
		this.bus_type = bus_type;
	}
	
	
}
