package vn.edu.vnuk.rediebus.model;

public class Price {
	private Long id;
    
	private Long routeId;
	private Long SeasonId;
	private Integer price;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRouteId() {
		return routeId;
	}
	public void setRouteId(Long routeId) {
		this.routeId = routeId;
	}
	public Long getSeasonId() {
		return SeasonId;
	}
	public void setSeasonId(Long seasonId) {
		SeasonId = seasonId;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	
	
    
}
