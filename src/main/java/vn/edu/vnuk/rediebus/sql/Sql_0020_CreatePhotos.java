package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class Sql_0020_CreatePhotos {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0020_CreatePhotos(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "CREATE TABLE photos("
						+ "id bigint primary key not null AUTO_INCREMENT,"
						+ "link nchar(50));";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("Table Photos is created!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
