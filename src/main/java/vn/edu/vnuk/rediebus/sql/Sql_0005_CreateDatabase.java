package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0005_CreateDatabase {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0005_CreateDatabase(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "Create database if not exists rediebus;";	
	}
	
		public void run() throws SQLException {
			
			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("The database 'rediebus' is created!");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			}
			
		}
	}