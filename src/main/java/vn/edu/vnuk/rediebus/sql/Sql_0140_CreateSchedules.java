package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0140_CreateSchedules {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0140_CreateSchedules(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "CREATE TABLE schedules("
						+ "id bigint primary key not null AUTO_INCREMENT,"
						+ "route_id bigint,"
						+ "day_id bigint,"
						+ "departure_time time,"
						+ "arrival_time time,"
						+ "foreign key(route_id) references routes(id),"
						+ "foreign key(day_id) references days(id)"
						+ ");";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("Table Schedules is created!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
