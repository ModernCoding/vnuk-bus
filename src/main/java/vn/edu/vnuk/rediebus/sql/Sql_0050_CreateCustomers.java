package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0050_CreateCustomers {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0050_CreateCustomers(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "CREATE TABLE customers("
						+ "id bigint primary key not null AUTO_INCREMENT,"
						+ "name nchar(100) not null, "
						+ "email nchar(50),"
						+ "phone nchar(50));";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("Table Customers is created!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
