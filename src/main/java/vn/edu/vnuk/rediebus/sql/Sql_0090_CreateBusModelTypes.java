package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0090_CreateBusModelTypes {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0090_CreateBusModelTypes(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "CREATE TABLE bus_model_types("
						+ "id bigint primary key not null AUTO_INCREMENT,"
						+ "bus_type_id bigint,"
						+ "bus_model_id bigint,"
						+ "photo_id bigint,"
						+ "foreign key(bus_type_id) references bus_types(id),"
						+ "foreign key(bus_model_id) references bus_models(id),"
						+ "foreign key(photo_id) references photos(id));";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("Table BusModelTypes is created!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
