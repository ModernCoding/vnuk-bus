package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0100_CreateBusTerminals {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0100_CreateBusTerminals(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "CREATE TABLE bus_terminals("
						+ "id bigint primary key not null AUTO_INCREMENT,"
						+ "city_id bigint,"
						+ "information text(500),"
						+ "bus_address nchar(50),"
						+ "phone_number nchar(50),"
						+ "photo_id  bigint,"
						+ "foreign key(city_id) references cities(id),"
						+ "foreign key(photo_id) references photos(id));";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("Table BusTerminals is created!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
