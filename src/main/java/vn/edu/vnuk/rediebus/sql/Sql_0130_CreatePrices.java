package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0130_CreatePrices {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0130_CreatePrices(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "CREATE TABLE prices("
						+ "id bigint primary key not null AUTO_INCREMENT,"
						+ "route_id bigint,"	
						+ "season_id bigint,"
						+ "price integer,"
						+ "foreign key(route_id) references routes(id),"
						+ "foreign key(season_id) references seasons(id));";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("Table Prices is created!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
