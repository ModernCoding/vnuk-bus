package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0001_DropDatabase {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0001_DropDatabase(Connection connection) {
		this.connection = connection;		
		this.sqlQuery = "Drop database if exists rediebus;";
			
	}
    
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("The database 'rediebus' is deleted!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
