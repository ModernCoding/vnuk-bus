package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0070_CreateCities {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0070_CreateCities(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "CREATE TABLE cities("
						+ "id bigint primary key not null AUTO_INCREMENT,"
						+ "name nchar(50),"
						+ "country_id bigint,"
						+ "foreign key(country_id) references countries(id));";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("Table Cities is created!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
