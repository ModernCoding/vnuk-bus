package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;

import vn.edu.vnuk.rediebus.jdbc.ConnectionFactory;


public class Sql_0000_RunAll {

	public static void main(String[] args) throws SQLException {
		
		Connection connectionDb = new ConnectionFactory().getConnection("jdbc:mysql://localhost");
		
		new Sql_0001_DropDatabase(connectionDb).run();
		new Sql_0005_CreateDatabase(connectionDb).run();
		
		connectionDb.close();
		
		Connection connectionTable = new ConnectionFactory().getConnection("jdbc:mysql://localhost/rediebus");
		
		new Sql_0010_CreateMessages(connectionTable).run();
		new Sql_0020_CreatePhotos(connectionTable).run();
		new Sql_0030_CreateBusManufactures(connectionTable).run();
		new Sql_0040_CreateBusTypes(connectionTable).run();
		new Sql_0050_CreateCustomers(connectionTable).run();
		new Sql_0060_CreateCountries(connectionTable).run();
		new Sql_0070_CreateCities(connectionTable).run();
		new Sql_0080_CreateBusModels(connectionTable).run();
		new Sql_0090_CreateBusModelTypes(connectionTable).run();
		new Sql_0100_CreateBusTerminals(connectionTable).run();
		new Sql_0110_CreateRoutes(connectionTable).run();
		new Sql_0120_CreateSeasons(connectionTable).run();
		new Sql_0130_CreatePrices(connectionTable).run();
		new Sql_0131_CreateDays(connectionTable).run();
		new Sql_0140_CreateSchedules(connectionTable).run();
		new Sql_0150_CreateTickets(connectionTable).run();
		
		connectionTable.close();
    }

}