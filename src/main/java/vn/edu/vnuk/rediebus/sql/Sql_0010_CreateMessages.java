package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0010_CreateMessages {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0010_CreateMessages(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "CREATE TABLE messages("
						+ "id bigint primary key not null AUTO_INCREMENT,"
						+ "customer_id bigint,"
						+ "messages text(1000));";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("Table Messages is created!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
