package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0150_CreateTickets {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0150_CreateTickets(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "CREATE TABLE tickets("
						+ "id bigint primary key not null AUTO_INCREMENT,"
						+ "customer_id bigint,"
						+ "phone_number nchar(50),"
						+ "email nchar(50),"
						+ "id_card_number nchar(50),"
						+ "route_id bigint,"
						+ "price_id bigint,"						
						+ "bus_type_id bigint,"	
						+ "schedule_id bigint,"
						+ "foreign key(customer_id) references customers(id),"
						+ "foreign key(route_id) references routes(id),"
						+ "foreign key(price_id) references prices(id),"
						+ "foreign key(bus_type_id) references bus_types(id),"
						+ "foreign key(schedule_id) references schedules(id)"
						+ ");";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("Table Tickets is created!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
