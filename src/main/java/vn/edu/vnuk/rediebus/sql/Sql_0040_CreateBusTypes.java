package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0040_CreateBusTypes {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0040_CreateBusTypes(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "CREATE TABLE bus_types("
						+ "id bigint primary key not null AUTO_INCREMENT,"
						+ "label nchar(100),"
						+ "information text(500));";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("Table BusTypes is created!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
