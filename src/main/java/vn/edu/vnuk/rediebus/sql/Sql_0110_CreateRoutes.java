package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0110_CreateRoutes {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0110_CreateRoutes(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "CREATE TABLE routes("
						+ "id bigint primary key not null AUTO_INCREMENT,"
						+ "departure_terminal bigint,"	
						+ "arrival_terminal bigint,"
						+ "information text(500),"
						+ "foreign key(departure_terminal) references bus_terminals(id),"
						+ "foreign key(arrival_terminal) references bus_terminals(id));";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("Table Routes is created!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
