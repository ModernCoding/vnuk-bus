package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0030_CreateBusManufactures {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0030_CreateBusManufactures(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "CREATE TABLE bus_manufactures("
						+ "id bigint primary key not null AUTO_INCREMENT,"
						+ "name nchar(255));";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("Table BusManufactures is created!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
