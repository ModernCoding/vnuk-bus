package vn.edu.vnuk.rediebus.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class Sql_0080_CreateBusModels {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql_0080_CreateBusModels(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "CREATE TABLE bus_models("
						+ "id bigint primary key not null AUTO_INCREMENT,"
						+ "name nchar(50),"
						+ "bus_manufacture_id bigint,"
						+ "foreign key(bus_manufacture_id) references bus_manufactures(id));";
	}

	
	public void run() throws SQLException {
		
		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("Table BusModels is created!");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		}
		
	}
}
