package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.rediebus.model.BusTerminal;

@Repository
public class BusTerminalDao {
	private Connection connection;

	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
	       
	}
    
    //  CREATE
    public void create(BusTerminal bus_terminal) throws SQLException{

        String sqlQuery = "insert into bus_terminals (city_id, information, bus_address, phone_number, photo_id) "
                        +	"values (?, ?, ?, ?, ?)";

        PreparedStatement statement;
        
        try {
            statement = connection.prepareStatement(sqlQuery);
            
//        	Replacing "?" through values
            
            statement.setLong(1, bus_terminal.getCityId());
            statement.setString(2, bus_terminal.getInformation());
            statement.setString(3, bus_terminal.getBusAddress());
            statement.setString(4, bus_terminal.getPhoneNumber());
            statement.setLong(5, bus_terminal.getPhotoId());
            
            // 	Executing statement
            statement.execute();

            System.out.println("New record in DB !");

    } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
    } finally {
            System.out.println("Done !");
            
    }

}
    //  READ (List of Tasks)
    @SuppressWarnings("finally")
    public List<BusTerminal> read() throws SQLException {

        String sqlQuery = "select * from bus_terminals";
        PreparedStatement statement;
        List<BusTerminal> bus_terminals = new ArrayList<BusTerminal>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                BusTerminal bus_terminal = new BusTerminal();
            	bus_terminal.setId(results.getLong("id"));
            	bus_terminal.setCityId(results.getLong("city_id"));
            	bus_terminal.setInformation(results.getString("information"));
            	bus_terminal.setBusAddress(results.getString("bus_address"));
            	bus_terminal.setPhoneNumber(results.getString("phone_number"));
            	bus_terminal.setPhotoId(results.getLong("photo_id"));
            	
            	bus_terminals.add(bus_terminal);
            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {      
                return bus_terminals;
        }


    }


    //  READ (Single Task)    
    @SuppressWarnings("finally")
    public BusTerminal read(int id) throws SQLException{

        String sqlQuery = "select * from bus_terminals where id=?";

        PreparedStatement statement;
        BusTerminal bus_terminals = new BusTerminal();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){
            	bus_terminals.setId(results.getLong("id"));
            	bus_terminals.setCityId(results.getLong("city_id"));
            	bus_terminals.setInformation(results.getString("information"));
            	bus_terminals.setBusAddress(results.getString("bus_address"));
            	bus_terminals.setPhoneNumber(results.getString("phone_number"));
            	bus_terminals.setPhotoId(results.getLong("photo_id"));
            }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
        	
            return bus_terminals;
        }

    }


    //  UPDATE
    public void update(BusTerminal bus_terminals) throws SQLException {
        String sqlQuery = "update bus_terminals set city_id=?, information=?, bus_address=?, phone_number=?, photo_id=?" 
                            + "where id=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, bus_terminals.getCityId());
            statement.setString(2, bus_terminals.getInformation());
            statement.setString(3, bus_terminals.getBusAddress());
            statement.setString(4, bus_terminals.getPhoneNumber());
            statement.setLong(5, bus_terminals.getPhotoId());
            
            statement.setLong(6, bus_terminals.getId());
            statement.execute();
            statement.close();
            
            System.out.println("Task successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from bus_terminals where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("BusTerminal successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
           
        }

    }
    }
