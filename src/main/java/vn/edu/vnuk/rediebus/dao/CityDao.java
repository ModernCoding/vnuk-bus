package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.rediebus.model.City;
import vn.edu.vnuk.rediebus.model.Country;

@Repository
public class CityDao {
	private Connection connection;

	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
	       
	}
    
    //  CREATE
    public void create(City city) throws SQLException{

        String sqlQuery = "insert into cities (name, country_id) "
                        +	"values (?, ?)";

        PreparedStatement statement;
        
        try {
            statement = connection.prepareStatement(sqlQuery);
            
//        	Replacing "?" through values
            
            statement.setString(1, city.getName());
            statement.setLong(2, city.getCountryId());
            
            // 	Executing statement
            statement.execute();

            System.out.println("New record in DB !");

    } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
    } finally {
            System.out.println("Done !");
            
    }

}
    //  READ (List of Tasks)
    @SuppressWarnings("finally")
    public List<City> read() throws SQLException {

        String sqlQuery = "select t1.id, t1.name, t2.id, t2.name "
        				+ "from cities t1, countries t2 "
        				+ "where t1.country_id = t2.id";
        
        PreparedStatement statement;
        List<City> cities = new ArrayList<City>();
        
        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            
            while(results.next()){

            	Country country = new Country();
                City city = new City();
                
                country.setId(results.getLong(3));
                country.setName(results.getString(4));
                
            	city.setId(results.getLong(1));
            	city.setName(results.getString(2));
            	city.setCountry(country);	
            	
            	cities.add(city);
            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {      
                return cities;
        }


    }


    //  READ (Single Task)    
    @SuppressWarnings("finally")
    public City read(int id) throws SQLException{

        String sqlQuery = "select * from cities where id=?";

        PreparedStatement statement;
        City city = new City();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){
            	city.setId(results.getLong("id"));
            	city.setName(results.getString("name"));
            	city.setCountryId(results.getLong("country_id")); 	
            }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
        	
            return city;
        }

    }


    //  UPDATE
    public void update(City city) throws SQLException {
        String sqlQuery = "update cities set name=?, country_id=?" 
                            + "where id=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, city.getName());
            statement.setLong(2, city.getCountryId());
            
            statement.setLong(3, city.getId());
            statement.execute();
            statement.close();
            
            System.out.println("City successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from cities where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("City successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
           
        }

    }
    }
