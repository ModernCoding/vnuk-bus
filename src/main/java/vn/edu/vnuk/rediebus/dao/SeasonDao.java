package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.rediebus.model.Season;

@Repository
public class SeasonDao {
	private Connection connection;

	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
    }


    //  CREATE
    public void create(Season season) throws SQLException{

        String sqlQuery = "insert into seasons (label,information) "+
                        	"values (?,?)";

        PreparedStatement statement;

        try {
                statement = connection.prepareStatement(sqlQuery);

                //	Replacing "?" through values
                statement.setString(1, season.getLabel());
                statement.setString(2, season.getInformation());
                // 	Executing statement
                statement.execute();

                System.out.println("New season in DB !");

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                System.out.println("Done !");
              
        }

    }
    
    
    //  READ (List of seasons)
    @SuppressWarnings("finally")
    public List<Season> read() throws SQLException {

        String sqlQuery = "select * from seasons";
        PreparedStatement statement;
        List<Season> seasons = new ArrayList<Season>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                Season season = new Season();
                season.setId(results.getLong("id"));
                season.setLabel(results.getString("label"));
                season.setInformation(results.getString("information"));
              
                seasons.add(season);
            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
              
                return seasons;
        }


    }


    //  READ (Single seasons)    
    @SuppressWarnings("finally")
    public Season read(int id) throws SQLException{

        String sqlQuery = "select * from seasons where id=?";

        PreparedStatement statement;
        Season season = new Season();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){
            	  season.setId(results.getLong("id"));
                  season.setLabel(results.getString("label"));
                  season.setInformation(results.getString("information"));

            }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
        	
            return season;
        }

    }


    //  UPDATE
    public void update(Season season) throws SQLException {
        String sqlQuery = "update seasons set label=? , information=?" +
                             "where id=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, season.getLabel());
            statement.setString(2, season.getInformation());
            statement.setLong(3, season.getId());
            statement.execute();
            statement.close();
            
            System.out.println("seasons successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
           
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from seasons where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("seasons successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }

    }
}