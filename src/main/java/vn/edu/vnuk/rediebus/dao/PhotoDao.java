package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.rediebus.model.Photo;

@Repository
public class PhotoDao {
	private Connection connection;

	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
    }


    //  CREATE
    public void create(Photo photos) throws SQLException{

        String sqlQuery = "insert into photos (link)"
                        	+"values (?)";

        PreparedStatement statement;

        try {
                statement = connection.prepareStatement(sqlQuery);

                //	Replacing "?" through values
                statement.setString(1, photos.getLink());
                
                // 	Executing statement
                statement.execute();

                System.out.println("New record in DB !");

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                System.out.println("Done !");
                
        }

    }
    
    //  READ (List of photos)
    @SuppressWarnings("finally")
    public List<Photo> read() throws SQLException {

        String sqlQuery = "select t1.id as photo_id, t1.link as photo_link, count(t2.id) as number_of_bus_model_types, count(t3.id) as number_of_bus_terminals "
				+ "from photos t1 "
				+ "left join bus_model_types t2 on t1.id = t2.bus_model_types_id, join bus_terminals t3 on t1.id = t3.bus_terminals_id "
				+ "group by t1.id";
        PreparedStatement statement;
        List<Photo> photos = new ArrayList<Photo>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

            	Photo  photo = new Photo();
                photo.setId(results.getLong("id"));
                photo.setLink(results.getString("link"));
                photo.setNumberOfBusModelTypes(results.getInt("number_of_bus_model_types"));
                photo.setNumberOfBusTerminals(results.getInt("number_of_bus_terminals"));
                             
                photos.add(photo);
            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                
                return photos;
        }


    }


    //  READ (Single photos)
    @SuppressWarnings("finally")
    public Photo read(int id) throws SQLException{

        String sqlQuery = "select * from photos where id=?";

        PreparedStatement statement;
        Photo photos = new Photo();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){

                photos.setId(results.getLong("id"));
                photos.setLink(results.getString("link"));
            }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
          
            return photos;
        }

    }


    //  UPDATE
    public void update(Photo photos) throws SQLException {
        String sqlQuery = "update photos set name=?" 
                             +"where id=?";
        
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, photos.getLink());
            statement.setLong(2, photos.getId());
            statement.execute();
            statement.close();
            
            System.out.println("photos successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from photos where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("photos successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }

    }
}