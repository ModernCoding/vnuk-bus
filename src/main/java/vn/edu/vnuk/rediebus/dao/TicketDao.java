package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.rediebus.model.Ticket;

@Repository
public class TicketDao {
	private Connection connection;

	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
	       
	}
    
    //  CREATE
    public void create(Ticket ticket) throws SQLException{

    	 String sqlQuery = "insert into tickets (customer_id, phone_number, email, id_card_number, route_id, price_id, bus_type_id, schedule_id) "
    			                         +	"values (?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement statement;
        
        try {
            statement = connection.prepareStatement(sqlQuery);
            
//        	Replacing "?" through values
            
            
            statement.setLong(1, ticket.getCustomerId());
            statement.setString(2, ticket.getPhoneNumber());
            statement.setString(3, ticket.getEmail());
            statement.setString(4, ticket.getIdCardNumber());
            statement.setLong(5, ticket.getRouteId());
            statement.setLong(6, ticket.getPriceId());
            statement.setLong(7, ticket.getBusTypeId());
            statement.setLong(8, ticket.getScheduleId());
           
            // 	Executing statement
            statement.execute();

            System.out.println("New tickets in DB !");

    } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
    } finally {
            System.out.println("Done !");
            
    }

}
    //  READ (List of tickets)
    @SuppressWarnings("finally")
    public List<Ticket> read() throws SQLException {

        String sqlQuery = "select * from tickets";
        PreparedStatement statement;
        List<Ticket> tickets = new ArrayList<Ticket>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                Ticket ticket = new Ticket();
            	ticket.setId(results.getLong("id"));
            	ticket.setCustomerId(results.getLong("customer_id"));
            	ticket.setPhoneNumber(results.getString("phone_number"));
            	ticket.setEmail(results.getString("email"));
            	ticket.setIdCardNumber(results.getString("id_card_number"));
            	ticket.setRouteId(results.getLong("route_id"));
            	ticket.setPriceId(results.getLong("price_id"));
            	ticket.setBusTypeId(results.getLong("bus_type_id"));
            	ticket.setScheduleId(results.getLong("schedule_id"));
            	
            	tickets.add(ticket);
            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {      
                return tickets;
        }


    }


    //  READ (Single tickets)    
    @SuppressWarnings("finally")
    public Ticket read(int id) throws SQLException{

        String sqlQuery = "select * from tickets where id=?";

        PreparedStatement statement;
        Ticket tickets = new Ticket();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){
            	tickets.setId(results.getLong("id"));
            	tickets.setCustomerId(results.getLong("customer_id"));
            	tickets.setPhoneNumber(results.getString("phone_number"));
            	tickets.setEmail(results.getString("email"));
            	tickets.setIdCardNumber(results.getString("id_card_number"));
            	tickets.setRouteId(results.getLong("route_id"));
            	tickets.setPriceId(results.getLong("price_id"));
            	tickets.setBusTypeId(results.getLong("bus_type_id"));
            	tickets.setScheduleId(results.getLong("schedule_id"));
             }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
        	
            return tickets;
        }

    }


    //  UPDATE
    public void update(Ticket tickets) throws SQLException {
        String sqlQuery = "update tickets set route_id=? , day_id=? , departure_time=? , arrival_time=?" 
                            + "where id=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
    
            statement.setLong(1, tickets.getCustomerId());
            statement.setString(2, tickets.getPhoneNumber());
            statement.setString(3, tickets.getEmail());
            statement.setString(4, tickets.getIdCardNumber());
            statement.setLong(5, tickets.getRouteId());
            statement.setLong(6, tickets.getPriceId());
            statement.setLong(7, tickets.getBusTypeId());
            statement.setLong(8, tickets.getScheduleId());
            statement.setLong(9, tickets.getId());
            statement.execute();
            statement.close();
            
            System.out.println("tickets successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from tickets where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("Ticket successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
           
        }

    }
    }
