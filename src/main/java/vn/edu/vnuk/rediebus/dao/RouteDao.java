package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.rediebus.model.Route;

@Repository
public class RouteDao {
	private Connection connection;


	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
    }


    //  CREATE
    public void create(Route routes) throws SQLException{

        String sqlQuery = "insert into routes (departure_terminal, arrival_terminal , information)"
                        	+"values (?,?,?)";

        PreparedStatement statement;

        try {
                statement = connection.prepareStatement(sqlQuery);

                //	Replacing "?" through values
                statement.setLong(1, routes.getArrivalTerminal());
                statement.setLong(2, routes.getDepartureTerminal());
                statement.setString(3, routes.getInformation());
                
                // 	Executing statement
                statement.execute();

                System.out.println("New routes in DB !");

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                System.out.println("Done !");
                
        }

    }
    
    
    //  READ (List of routes)
    @SuppressWarnings("finally")
    public List<Route> read() throws SQLException {

        String sqlQuery = "select * from routes";
        PreparedStatement statement;
        List<Route> routes = new ArrayList<Route>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

            	Route route = new Route();
                route.setId(results.getLong("id"));
                route.setArrivalTerminal(results.getLong("arrival_terminal"));
                route.setDepartureTerminal(results.getLong("departure_terminal"));
                route.setInformation(results.getString("information"));
            

          
                routes.add(route);

            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                
                return routes;
        }


    }


    //  READ (Single routes)
    @SuppressWarnings("finally")
    public Route read(int id) throws SQLException{

        String sqlQuery = "select * from routes where id=?";

        PreparedStatement statement;
        Route routes = new Route();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){

                routes.setId(results.getLong("id"));
                routes.setArrivalTerminal(results.getLong("arrival_terminal"));
                routes.setDepartureTerminal(results.getLong("departure_terminal"));
                routes.setInformation(results.getString("information"));
            }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
          
            return routes;
        }

    }


    //  UPDATE
    public void update(Route routes) throws SQLException {
        String sqlQuery = "update routes set arrival_terminal=? ,departure_terminal=? , information=?" 
                             +"where id=?";
        
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, routes.getArrivalTerminal());
            statement.setLong(2, routes.getDepartureTerminal());
            statement.setString(3, routes.getInformation());
            statement.setLong(4, routes.getId());
            statement.execute();
            statement.close();
            
            System.out.println("routes successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from routes where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("routes successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }

    }
}