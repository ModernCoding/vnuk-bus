package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.rediebus.model.Customer;

@Repository
public class CustomerDao {
	private Connection connection;

	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
	       
	}
    
    //  CREATE
    public void create(Customer customer) throws SQLException{

        String sqlQuery = "insert into customers (name, email, phone) "
                        +	"values (?, ?, ?)";

        PreparedStatement statement;
        
        try {
            statement = connection.prepareStatement(sqlQuery);
            
//        	Replacing "?" through values
            
            statement.setString(1, customer.getName());
            statement.setString(2, customer.getEmail());
            statement.setString(3, customer.getPhone());
            
            // 	Executing statement
            statement.execute();

            System.out.println("New record in DB !");

    } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
    } finally {
            System.out.println("Done !");
            
    }

}
    //  READ (List of Tasks)
    @SuppressWarnings("finally")
    public List<Customer> read() throws SQLException {

        String sqlQuery = "select * from customers";
        PreparedStatement statement;
        List<Customer> customers = new ArrayList<Customer>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                Customer customer = new Customer();
            	customer.setId(results.getLong("id"));
            	customer.setName(results.getString("name"));
            	customer.setEmail(results.getString("email"));
            	customer.setPhone(results.getString("phone"));
            	
            	customers.add(customer);
            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {      
                return customers;
        }


    }


    //  READ (Single Task)    
    @SuppressWarnings("finally")
    public Customer read(int id) throws SQLException{

        String sqlQuery = "select * from customers where id=?";

        PreparedStatement statement;
        Customer customers = new Customer();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){
            	customers.setId(results.getLong("id"));
                customers.setName(results.getString("name"));
                customers.setEmail(results.getString("email"));
                customers.setPhone(results.getString("phone"));
            }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
        	
            return customers;
        }

    }


    //  UPDATE
    public void update(Customer customers) throws SQLException {
        String sqlQuery = "update customers set name=?, email=?, phone=?" 
                            + "where id=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, customers.getName());
            statement.setString(2, customers.getEmail());
            statement.setString(3, customers.getPhone());
            
            statement.setLong(4, customers.getId());
            statement.execute();
            statement.close();
            
            System.out.println("Task successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from customers where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("Customer successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
           
        }

    }
    }
