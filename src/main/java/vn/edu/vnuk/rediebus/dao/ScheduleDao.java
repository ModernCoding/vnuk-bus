package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.rediebus.model.Schedule;

@Repository
public class ScheduleDao {
	private Connection connection;

	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
	       
	}
    
    //  CREATE
    public void create(Schedule schedule) throws SQLException{

    	 String sqlQuery = "insert into schedules (route_id, day_id, departure_time, arrival_time) "
    			                         +	"values (?, ?, ?, ?)";

        PreparedStatement statement;
        
        try {
            statement = connection.prepareStatement(sqlQuery);
            
//        	Replacing "?" through values
            
            
            statement.setLong(1, schedule.getRouteId());
            statement.setLong(2, schedule.getDayId());
            statement.setTime(3, schedule.getDepartureTime());
            statement.setTime(4, schedule.getArrivalTime());
           
            // 	Executing statement
            statement.execute();

            System.out.println("New schedules in DB !");

    } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
    } finally {
            System.out.println("Done !");
            
    }

}
    //  READ (List of scheduless)
    @SuppressWarnings("finally")
    public List<Schedule> read() throws SQLException {

        String sqlQuery = "select * from schedules";
        PreparedStatement statement;
        List<Schedule> schedules = new ArrayList<Schedule>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                Schedule schedule = new Schedule();
            	schedule.setId(results.getLong("id"));
            	schedule.setDayId(results.getLong("day_id"));
            	schedule.setDepartureTime(results.getTime("departure_time"));
            	schedule.setArrivalTime(results.getTime("arrival_time"));
            	schedules.add(schedule);
            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {      
                return schedules;
        }


    }


    //  READ (Single schedules)    
    @SuppressWarnings("finally")
    public Schedule read(int id) throws SQLException{

        String sqlQuery = "select * from schedules where id=?";

        PreparedStatement statement;
        Schedule schedules = new Schedule();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){
            	schedules.setId(results.getLong("id"));
            	schedules.setDayId(results.getLong("day_id"));
            	schedules.setDepartureTime(results.getTime("departure_time"));
            	schedules.setArrivalTime(results.getTime("arrival_time"));
             }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
        	
            return schedules;
        }

    }


    //  UPDATE
    public void update(Schedule schedules) throws SQLException {
        String sqlQuery = "update schedules set route_id=? , day_id=? , departure_time=? , arrival_time=?" 
                            + "where id=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
    
            statement.setLong(1, schedules.getRouteId());
            statement.setLong(2, schedules.getDayId());
            statement.setTime(3, schedules.getDepartureTime());
            statement.setTime(4, schedules.getArrivalTime());
            statement.setLong(5, schedules.getId());
            statement.execute();
            statement.close();
            
            System.out.println("schedules successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from schedules where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("Schedule successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
           
        }

    }
    }
