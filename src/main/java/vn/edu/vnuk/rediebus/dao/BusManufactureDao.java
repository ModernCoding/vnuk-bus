package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;
import vn.edu.vnuk.rediebus.model.BusManufacture;

@Repository
public class BusManufactureDao {
	private Connection connection;

	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
	       
	}
    
    //  CREATE
    public void create(BusManufacture bus_manufacture) throws SQLException{

        String sqlQuery = "insert into bus_manufactures (name) "
                        +	"values (?)";

        PreparedStatement statement;
        
        try {
            statement = connection.prepareStatement(sqlQuery);
            
//        	Replacing "?" through values
            
            statement.setString(1, bus_manufacture.getName());
                       
            // 	Executing statement
            statement.execute();

            System.out.println("New record in DB !");

    } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
    } finally {
            System.out.println("Done !");
            
    }

}
    //  READ (List of Tasks)
    @SuppressWarnings("finally")
    public List<BusManufacture> read() throws SQLException {

        String sqlQuery = "select t1.id as bus_manufacture_id, t1.name as bus_manufacture_name, count(t2.id) as number_of_bus_models "
				+ "from bus_manufactures t1 "
				+ "left join bus_models t2 on t1.id = t2.bus_manufacture_id "
				+ "group by t1.id;";
        
        PreparedStatement statement;
        List<BusManufacture> bus_manufactures = new ArrayList<BusManufacture>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                BusManufacture bus_manufacture = new BusManufacture();
            	bus_manufacture.setId(results.getLong("bus_manufacture_id"));
            	bus_manufacture.setName(results.getString("bus_manufacture_name"));
            	bus_manufacture.setNumberOfBusModels(results.getInt("number_of_bus_models"));
            	
            	bus_manufactures.add(bus_manufacture);
            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {      
                return bus_manufactures;
        }


    }


    //  READ (Single Task)    
    @SuppressWarnings("finally")
    public BusManufacture read(int id) throws SQLException{

        String sqlQuery = "select * from bus_manufactures where id=?";

        PreparedStatement statement;
        BusManufacture bus_manufactures = new BusManufacture();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){
            	bus_manufactures.setId(results.getLong("id"));
                bus_manufactures.setName(results.getString("name"));
             }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
        	
            return bus_manufactures;
        }

    }


    //  UPDATE
    public void update(BusManufacture bus_manufactures) throws SQLException {
        String sqlQuery = "update bus_manufactures set name=?" 
                            + "where id=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, bus_manufactures.getName());
           
            statement.setLong(2, bus_manufactures.getId());
            statement.execute();
            statement.close();
            
            System.out.println("Task successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from bus_manufactures where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("BusManufacture successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
           
        }

    }
    }
