package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.rediebus.model.BusModelType;

@Repository
public class BusModelTypeDao {
	private Connection connection;

	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
	       
	}
    
    //  CREATE
    public void create(BusModelType bus_model_type) throws SQLException{

    	String sqlQuery = "insert into bus_model_types (bus_type_id, bus_model_id, photo_id) "
    			                       +	"values (?, ?, ?)";

        PreparedStatement statement;
        
        try {
            statement = connection.prepareStatement(sqlQuery);
            
//        	Replacing "?" through values
            
            statement.setLong(1,bus_model_type.getBusTypeId());
            statement.setLong(2,bus_model_type.getBusModelId());
            statement.setLong(3,bus_model_type.getPhotoId());
                       
            // 	Executing statement
            statement.execute();

            System.out.println("New bus_model_types in DB !");

    } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
    } finally {
            System.out.println("Done !");
            
    }

}
    //  READ (List of bus_model_typess)
    @SuppressWarnings("finally")
    public List<BusModelType> read() throws SQLException {

        String sqlQuery =  "select t1.id, t2.id, t2.label, t3.id t3.name, t4.id t4.link "
				+ "from bus_model_types t1, bus_types t2, bus_models t3, photos t4 "
				+ "where t1.bus_type_id = t2.id, t1.bus_model_id = t3.id, t1.photo_id = t4.id";
        PreparedStatement statement;
        List<BusModelType> bus_model_types = new ArrayList<BusModelType>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                BusModelType bus_model_type = new BusModelType();
                bus_model_type.setId(results.getLong("id"));
               	bus_model_type.setBusTypeId(results.getLong("bus_type_id"));
               	bus_model_type.setBusModelId(results.getLong("bus_model_id"));
            	bus_model_type.setPhotoId(results.getLong("photo_id"));
            	bus_model_types.add(bus_model_type);
            	
            	bus_model_types.add(bus_model_type);
            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {      
                return bus_model_types;
        }


    }


    //  READ (Single bus_model_types)    
    @SuppressWarnings("finally")
    public BusModelType read(int id) throws SQLException{

        String sqlQuery = "select * from bus_model_types where id=?";

        PreparedStatement statement;
        BusModelType bus_model_types = new BusModelType();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){
            	bus_model_types.setId(results.getLong("id"));
            	bus_model_types.setBusTypeId(results.getLong("bus_type_id"));
            	bus_model_types.setBusModelId(results.getLong("bus_model_id"));
            	bus_model_types.setPhotoId(results.getLong("photo_id"));
             }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
        	
            return bus_model_types;
        }

    }


    //  UPDATE
    public void update(BusModelType bus_model_types) throws SQLException {
    	String sqlQuery = "update bus_model_types set bus_type_id=?, bus_model_id=?,photo_id=?" 
    			                            +  "where id=?";
    			        try {
    			            PreparedStatement statement = connection.prepareStatement(sqlQuery);
    			            statement.setLong(1,bus_model_types.getBusTypeId());
    			            statement.setLong(2,bus_model_types.getBusModelId());
    			            statement.setLong(3,bus_model_types.getPhotoId());
    			            
    			            statement.setLong(4, bus_model_types.getId());
    			            statement.execute();
    			            statement.close();
            
            System.out.println("	 successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from bus_model_types where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("BusModelType successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
           
        }

    }
    }
