package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.rediebus.model.Message;

@Repository
public class MessageDao {
	private Connection connection;

	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
	       
	}
    
    //  CREATE
    public void create(Message message) throws SQLException{

        String sqlQuery = "insert into messages (messages, customer_id) "
                        +	"values (?, ?)";

        PreparedStatement statement;
        
        try {
            statement = connection.prepareStatement(sqlQuery);
            
//        	Replacing "?" through values
            
            statement.setString(1, message.getMessages());
            statement.setLong(2, message.getCustomer_id());
            
            // 	Executing statement
            statement.execute();

            System.out.println("New record in DB !");

    } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
    } finally {
            System.out.println("Done !");
            
    }

}
    //  READ (List of Tasks)
    @SuppressWarnings("finally")
    public List<Message> read() throws SQLException {

        String sqlQuery = "select * from messages";
        PreparedStatement statement;
        List<Message> messages = new ArrayList<Message>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                Message message = new Message();
            	message.setId(results.getLong("id"));
            	message.setMessages(results.getString("messages"));
            	message.setCustomer_id(results.getLong("customer_id"));
                        	
            	messages.add(message);
            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {      
                return messages;
        }


    }


    //  READ (Single Task)    
    @SuppressWarnings("finally")
    public Message read(int id) throws SQLException{

        String sqlQuery = "select * from messages where id=?";

        PreparedStatement statement;
        Message messages = new Message();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){
            	messages.setId(results.getLong("id"));
            	messages.setMessages(results.getString("messages"));
            	messages.setCustomer_id(results.getLong("customer_id"));
            }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
        	
            return messages;
        }

    }


    //  UPDATE
    public void update(Message messages) throws SQLException {
        String sqlQuery = "update messages set messages=?, customer_id=? " 
                            + "where id=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, messages.getMessages());
            statement.setLong(2, messages.getCustomer_id());
            
            statement.setLong(3, messages.getId());
            statement.execute();
            statement.close();
            
            System.out.println("Task successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from messages where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("Message successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
           
        }

    }
    }
