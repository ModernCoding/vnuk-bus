package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.rediebus.model.BusManufacture;
import vn.edu.vnuk.rediebus.model.BusModel;

@Repository
public class BusModelDao {
	private Connection connection;

	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
	       
	}
    
    //  CREATE
    public void create(BusModel bus_model) throws SQLException{

        String sqlQuery = "insert into bus_models (name, bus_manufacture_id) "
                        +	"values (?, ?)";

        PreparedStatement statement;
        
        try {
            statement = connection.prepareStatement(sqlQuery);
            
//        	Replacing "?" through values
            
            statement.setString(1, bus_model.getName());
            statement.setLong(2, bus_model.getBusManufactureId());
            
            // 	Executing statement
            statement.execute();

            System.out.println("New record in DB !");

    } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
    } finally {
            System.out.println("Done !");
            
    }

}
    //  READ (List of Tasks)
    @SuppressWarnings("finally")
    public List<BusModel> read() throws SQLException {

    	 String sqlQuery = "select t1.id as bus_model_id, t1.name as bus_model_name, t2.id, t2.name "
 				+ "from bus_models t1, bus_manufactures t2 "
 				+ "where t1.bus_manufacture_id = t2.id";
        PreparedStatement statement;
        List<BusModel> bus_models = new ArrayList<BusModel>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                BusModel bus_model = new BusModel();
                BusManufacture bus_manufacture = new BusManufacture();
                
                bus_manufacture.setId(results.getLong(1));
                bus_manufacture.setName(results.getString(2));
                
            	bus_model.setId(results.getLong(3));
            	bus_model.setName(results.getString(4));
            	bus_model.setBus_manufacture(bus_manufacture);
            	
            	bus_models.add(bus_model);
            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {      
                return bus_models;
        }


    }


    //  READ (Single Task)    
    @SuppressWarnings("finally")
    public BusModel read(int id) throws SQLException{

        String sqlQuery = "select * from bus_models where id=?";

        PreparedStatement statement;
        BusModel bus_model = new BusModel();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){
            	bus_model.setId(results.getLong("id"));
            	bus_model.setName(results.getString("name"));
            	bus_model.setBusManufactureId(results.getLong("bus_manufacture_id")); 	
            }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
        	
            return bus_model;
        }

    }


    //  UPDATE
    public void update(BusModel bus_model) throws SQLException {
        String sqlQuery = "update bus_models set name=?, country_id=?" 
                            + "where id=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, bus_model.getName());
            statement.setLong(2, bus_model.getBusManufactureId());
            
            statement.setLong(3, bus_model.getId());
            statement.execute();
            statement.close();
            
            System.out.println("BusModel successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from bus_models where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("BusModel successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
           
        }

    }
    }
