package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;
import vn.edu.vnuk.rediebus.model.Country;

@Repository
public class CountryDao {
	private Connection connection;

	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
    }


    //  CREATE
    public void create(Country country) throws SQLException{

        String sqlQuery = "insert into countries (name) "+
                        	"values (?)";

        PreparedStatement statement;

        try {
                statement = connection.prepareStatement(sqlQuery);

                //	Replacing "?" through values
                statement.setString(1, country.getName());
             
                // 	Executing statement
                statement.execute();

                System.out.println("New country in DB !");

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                System.out.println("Done !");
              
        }

    }
    
    
    //  READ (List of countries)
    @SuppressWarnings("finally")
    public List<Country> read() throws SQLException {

        String sqlQuery = "select t1.id as country_id, t1.name as country_name, count(t2.id) as number_of_cities "
        				+ "from countries t1 "
        				+ "left join cities t2 on t1.id = t2.country_id "
        				+ "group by t1.id;";
        
        PreparedStatement statement;
        List<Country> countries = new ArrayList<Country>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

            	Country country = new Country();
                country.setId(results.getLong("country_id"));
	            country.setName(results.getString("country_name"));
	            country.setNumberOfCities(results.getInt("number_of_cities"));
	            
	            
	            countries.add(country);
            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
              
                return countries;
        }


    }


    //  READ (Single countries)    
    @SuppressWarnings("finally")
    public Country read(int id) throws SQLException{

        String sqlQuery = "select * from countries where id=?";

        PreparedStatement statement;
        Country country = new Country();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){
            	  country.setId(results.getLong("id"));
                  country.setName(results.getString("name"));
               

            }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
        	
            return country;
        }

    }


    //  UPDATE
    public void update(Country country) throws SQLException {
        String sqlQuery = "update countries set name=?" +
                             "where id=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, country.getName());
          
            
            statement.setLong(2, country.getId());
            statement.execute();
            statement.close();
            
            System.out.println("countries successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
           
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from countries where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("countries successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }

    }
}