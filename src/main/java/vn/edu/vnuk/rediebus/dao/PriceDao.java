package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.rediebus.model.Price;

@Repository
public class PriceDao {
	private Connection connection;


	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
    }


    //  CREATE
    public void create(Price prices) throws SQLException{

        String sqlQuery = "insert into prices (route_id, season_id,price)"
                        	+"values (?,?,?)";

        PreparedStatement statement;

        try {
                statement = connection.prepareStatement(sqlQuery);

                //	Replacing "?" through values
                statement.setLong(1, prices.getRouteId());
                statement.setLong(2, prices.getSeasonId());
                statement.setInt(3, prices.getPrice());
                // 	Executing statement
                statement.execute();

                System.out.println("New prices in DB !");

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                System.out.println("Done !");
                
        }

    }
    
    
    //  READ (List of prices)
    @SuppressWarnings("finally")
    public List<Price> read() throws SQLException {

        String sqlQuery = "select * from prices";
        PreparedStatement statement;
        List<Price> prices = new ArrayList<Price>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

            	Price price = new Price();
                price.setId(results.getLong("id"));
                price.setRouteId(results.getLong("route_id"));
                price.setSeasonId(results.getLong("season_id"));
                price.setPrice(results.getInt("price"));
            

          
                prices.add(price);

            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                
                return prices;
        }


    }


    //  READ (Single prices)
    @SuppressWarnings("finally")
    public Price read(int id) throws SQLException{

        String sqlQuery = "select * from prices where id=?";

        PreparedStatement statement;
        Price prices = new Price();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){

            	 prices.setId(results.getLong("id"));
                 prices.setRouteId(results.getLong("route_id"));
                 prices.setSeasonId(results.getLong("season_id"));
                 prices.setPrice(results.getInt("price"));
            }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
          
            return prices;
        }

    }


    //  UPDATE
    public void update(Price prices) throws SQLException {
        String sqlQuery = "update prices set route_id=? , season_id=?,price=?" 
                             +"where id=?";
        
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, prices.getRouteId());
            statement.setLong(2, prices.getSeasonId());
            statement.setInt(3, prices.getPrice());
            statement.setLong(4, prices.getId());
            statement.execute();
            statement.close();
            
            System.out.println("prices successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from prices where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("prices successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }

    }
}