package vn.edu.vnuk.rediebus.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.rediebus.model.BusType;

@Repository
public class BusTypeDao {
	private Connection connection;


	public void setDataSource(DataSource dataSource) {
	       
	       try {
		    	this.connection = dataSource.getConnection();
		    } catch (SQLException e) {
		    	throw new RuntimeException(e);
		    }
    }


    //  CREATE
    public void create(BusType types) throws SQLException{

        String sqlQuery = "insert into bus_types (label, information)"
                        	+"values (?,?)";

        PreparedStatement statement;

        try {
                statement = connection.prepareStatement(sqlQuery);

                //	Replacing "?" through values
                statement.setString(1, types.getLabel());
                statement.setString(2, types.getInformation());
                
                // 	Executing statement
                statement.execute();

                System.out.println("New record in DB !");

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                System.out.println("Done !");
                
        }

    }
    
    
    //  READ (List of bus_types)
    @SuppressWarnings("finally")
    public List<BusType> read() throws SQLException {

        String sqlQuery = "select t1.id as bus_type_id, t1.label as bus_type_label, count(t2.id) as number_of_bus_model_types, count(t3.id) as number_of_tickets "
				+ "from bus_types t1 "
				+ "left join bus_model_types t2 on t1.id = t2.bus_type_id, join tickets on t1.id = t3.bus_type_id "
				+ "group by t1.id";
        PreparedStatement statement;
        List<BusType> bus_types = new ArrayList<BusType>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

            	BusType  types = new BusType();
                types.setId(results.getLong("id"));
                types.setLabel(results.getString("label"));
                types.setInformation(results.getString("information"));
                types.setNumberOfBusModelTypes(results.getInt("number_of_bus_model_types"));
                types.setNumberOfTickets(results.getInt("number_of_tickets"));

                bus_types.add(types);

            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                
                return bus_types;
        }


    }


    //  READ (Single bus_types)
    @SuppressWarnings("finally")
    public BusType read(int id) throws SQLException{

        String sqlQuery = "select * from bus_types where id=?";

        PreparedStatement statement;
        BusType types = new BusType();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){

                types.setId(results.getLong("id"));
                types.setLabel(results.getString("label"));
                types.setInformation(results.getString("information"));
            }

            statement.close();

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
          
            return types;
        }

    }


    //  UPDATE
    public void update(BusType types) throws SQLException {
        String sqlQuery = "update bus_types set label=? , information=?" 
                             +"where id=?";
        
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, types.getLabel());
            statement.setString(2, types.getInformation());
            statement.setLong(3, types.getId());
            statement.execute();
            statement.close();
            
            System.out.println("bus_types successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }
        
    }
    
    
    //  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from bus_types where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("bus_types successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        finally {
            
        }

    }
}