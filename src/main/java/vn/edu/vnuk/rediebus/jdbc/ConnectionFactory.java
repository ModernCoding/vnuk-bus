package vn.edu.vnuk.rediebus.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	public Connection getConnection(String url) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			return DriverManager.getConnection(
					url, 
					"root", 
					System.getenv("PW_DB")
				);
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
